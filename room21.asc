// room script file
int tdel=0;

function room_Load()
{
  SetWalkBehindBase_New(1, 359);
  SetWalkBehindBase_New(2, 222);
  
  pickupvar=0;
   cEgo.UnlockView();
  cEgo.x=259;
  cEgo.y=345;
  cEgo.FaceDirection(eDirectionUp, eNoBlock);
  
  oTeratomalight2.SetView(TLIGHTS, 0, 0);
  oTeratomalight2.Animate(0, 3, eRepeat, eNoBlock, eForwards);
  
  if (cEgo.PreviousRoom==-1)
  {
    hasnotpressedstart=false;
    cEgo.PlaceOnWalkableArea();
    cEgo.AddInventory(iDagger);
    //complete_sequence=true;
  }
  
  if (Game.DoOnceOnly("SetTeraButtons"))
  {
    int i=1;
    while (i < gTeratoma.ControlCount-1)
    {
      gTeratoma.Controls[i].AsButton.X-=100;
      
      gTeratoma.Controls[i].AsButton.Y-=30;
      i++;
    }
  }
  
}

function repeatedly_execute_always()
{
  tdel++;
  if (tdel>2)
  {
    OTeratomalight.Transparency=5+Random(5);
    tdel=0;
  }
  
  if (TeratomaEyex && cTeratom.View!=TERATIDLENO)
  {
    cTeratom.LockViewFrame(TERATIDLENO, 0, 0);
  }
  
}
function room_Leave()
{ 
}

function hExit_Interact()
{
  cEgo.ChangeRoomz(8, 115, 260);
}



function room_AfterFadeIn()
{
}

function on_call (int value) {
  if (value == 1) 
  {
    cEgo.FaceDirection(eDirectionUpLeft, eBlock);
      
      
      cEgo.LockViewFrame(EGOHONEDCUT, 1, 0);
      cEgo.Animate(1, 3, eOnce, eNoBlock, eForwards);
      bool donedidit=false;
      while (cEgo.Animating)
      {
        if (cEgo.Frame==7 &&!donedidit)
        {
          SFX_Play(_TouchMouth1b, 0);//aTouchMouth1b.Play();
          donedidit=true;
          TeratomaEyex=true;
        } 
        WaitC(1);
      }
      cEgo.UnlockView();
      cEgo.FaceDirection(eDirectionUpLeft, eBlock);
      
      SFX_Play(_Getitem1a, 0);//agetitem1a     
      WaitC(25);     
      cEgo.Speak("What, did you think I was here to play games with you?");
  }
}


function room_RepExec()
{
  if (Pause)
  {
    return;
  }
}
