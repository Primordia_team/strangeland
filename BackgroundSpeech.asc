// new module script
struct Message
{
  Character *Talking;  
  String Text;
  bool Enabled;
  int Duration;
  int cRoom;
};

//duration is grabbed from text length
//Character color from Talking Character

Message Messages[40];
int MaxMessages=40;

DynamicSprite*BGSprite;







function BackgroundSpeech(this Character*, String txt)
{
  int i=0;
  int setId=-1;
   
  
  
  while (i < MaxMessages)
  {
    if (Messages[i].Enabled==false)
    {
      setId=i;
      i=2000;
    }
    i++;
  }
  
  if (setId==-1)
  {
    return;
  }
  
  
  Messages[setId].Enabled=true;
  Messages[setId].Talking=this;
  Messages[setId].Text=txt; 
    Messages[setId].cRoom=player.Room;
  
  //CALCULATE DURATION
  int c=0;
  int topspaces=0;
  int setTotalDuration=0;
  while (c < txt.Length)
  {
    if (txt.Chars[c]==' ' || txt.Chars[c]=='.') 
    {
      if (Messages[setId].cRoom==6 || Messages[setId].cRoom==9)
      {
        if (topspaces<12)
        {
          topspaces++;
          setTotalDuration+=Game.TextReadingSpeed/2;
        }
      }
      else 
      {
        setTotalDuration+=Game.TextReadingSpeed/2;
      }
    }
    else if (txt.Chars[c]==',' || txt.Chars[c]=='-')setTotalDuration+=Game.TextReadingSpeed/3;
    else setTotalDuration+=Game.TextReadingSpeed/5;
    c++;
  }
  if (setTotalDuration<40) setTotalDuration=20;
  
  
  Messages[setId].Duration=setTotalDuration;
 
}

function game_start()
{
  cSpeakerText.Transparency=100;
  growl=DynamicSprite.Create(640, 360, true);  
  
}

bool MessageUp=false;
int UpdateY=0;
bool GlitchVal=false;
int GX=0;

function Parse()
{
  int i=0;
  int setId=-1;
  int graber2 = cSpeakerText.Transparency;

  if (!MessageUp)
  {    
    if (graber2+4<=100)
    {
      cSpeakerText.Transparency=graber2+4;  
      if (player.Room!=13) cSpeakerText.x=GX+GetViewportX();
      else cSpeakerText.x=GX+(GetViewportX()/2);
    }
    else 
    {
      cSpeakerText.Transparency=100;
    }    
  }
  else 
  {
    if (DeadNpc)
    {
      if (cSpeakerText.Transparency<100) cSpeakerText.Transparency+=1;
      else cSpeakerText.Transparency=100;
    }
    else cSpeakerText.Transparency=0;  
  }
  
  //cSpeakerText.x=cSpeakerText.x+GetViewportX();
  
  
  
  if (player.Room!=21 && player.Room!=9 && player.Room!=6 && player.Room!=13)
  {
    
    cSpeakerText.Transparency=100;
    MessageUp=false;
    return;
  }
  
  
  i=0;
  
  while (i < MaxMessages)
  {
    if (Messages[i].Enabled==true && Messages[i].Duration>0 && Messages[i].cRoom==player.Room)
    {
      if (Messages[i].Text==" ")
      {
        MessageUp=false;
        Messages[i].Duration--;
        
        return;
        
      }
      else 
      {
      int bwidth=640;// GetTextWidth(Messages[i].Text, Game.SpeechFont)+20;
      int aTwidth=GetTextWidth(Messages[i].Text, Game.SpeechFont);
      
      aTwidth=2;
      
      
      //aTwidth=80;
      int bx=0;//20;
      int by=310;//300;
      
      if (BGSprite==null)
      {
        BGSprite=DynamicSprite.Create(640, 360, true);              
      }
      
      DrawingSurface*area = BGSprite.GetDrawingSurface();
      area.Clear(COLOR_TRANSPARENT);
      area.DrawingColor=ColorPurple;//Messages[i].Talking.SpeechColor;
      if (player.Room==13) area.DrawStringWrapped(bx, by, bwidth, Game.SpeechFont, eAlignCentre, Messages[i].Text);//eAlignCentre
      else if (player.Room==6 ||player.Room==9) 
      {
       // area.DrawStringWrapped(bx, by, bwidth, Game.SpeechFont, eAlignRight, Messages[i].Text);//eAlignCentre
      }
      else area.DrawStringWrapped(bx, by, bwidth, Game.SpeechFont, eAlignLeft, Messages[i].Text);//eAlignCentre
      
      int denX=20;
      if (player.Room==6 || player.Room==9)
      {
        String al=Messages[i].Text;
        int d=0;
        bool found_Ex=false;
        while (d < Messages[i].Text.Length)
        {
          if (Messages[i].Text.Chars[d]=='[')
          {
            found_Ex=true;
            String subst = Messages[i].Text.Substring(0, d);
            String subst2 = Messages[i].Text.Substring(d+1, Messages[i].Text.Length);

            area.DrawStringWrapped(bx, by, bwidth, Game.SpeechFont, eAlignRight, subst);//eAlignCentre
            //cEgo.SayBackground(subst2);
            if (subst2=="for want of wings and souls.")
            {
              area.DrawStringWrapped(bx+164, by+15, bwidth, Game.SpeechFont, eAlignCentre, subst2);
            }
            else area.DrawStringWrapped(bx, by+15, bwidth, Game.SpeechFont, eAlignRight, subst2);
            al=subst;
          }
          d++;
        }
        if (!found_Ex)
        {
          area.DrawStringWrapped(bx, by, bwidth, Game.SpeechFont, eAlignRight, Messages[i].Text);//eAlignCentre
        }
        
        int getWidth=GetTextWidth(al, Game.SpeechFont);
        denX=getWidth-210;
        
      }
      area.Release();
      
      
      
      
      if (player.Room==9) 
      {        
        denX-=20;
        int arb=(cSpeakerText.Scaling-100)/2;
        cSpeakerText.y=270+arb;//100-108
      }
      else if (player.Room==13) 
      {
       
        if (!SlideShowCloseup)
        {
          denX=650-215;
          cSpeakerText.y=105;
        }
        else
        {
          denX=710-210;
          
          if (cSlideBG.Transparency==0) cSpeakerText.y=65;//105;
          else cSpeakerText.y=6500;//105;
          
          
        }
      }
      else if (player.Room==6) 
      {
        
        int arb=(cSpeakerText.Scaling-100)/2;
        cSpeakerText.y=105+4+arb;
      }
      else if (player.Room==21) 
      {
        denX=550;
        cSpeakerText.y=105;
      }
      else
      {
        denX=380;
        cSpeakerText.y=360;
      }
      
      if (player.Room!=13) 
      {
        if (player.Room==9 || player.Room==6)
        {
          GX=denX;
          cSpeakerText.x=(GX)+GetViewportX();//360;
        }
        else 
        {
          GX=(denX+(aTwidth/2));
          cSpeakerText.x=(denX+(aTwidth/2))+GetViewportX();//360;
        }
      }
      else 
      {
        GX=(denX+(aTwidth/2));
        cSpeakerText.x=(denX+(aTwidth/2))+(GetViewportX()/2);
      }
      
      
      
      MessageUp=true;
      ViewFrame*frer = Game.GetViewFrame(cSpeakerText.NormalView, 0, 0);      
      frer.Graphic=BGSprite.Graphic;
      //gSpeakers.BackgroundGraphic= BGSprite.Graphic;
      
      
      Messages[i].Duration--;
      return;
      }
    }
    if (Messages[i].Enabled==true && Messages[i].Duration==0)
    {
      Messages[i].Enabled=false;
      Messages[i].Text=null; 
      
      MessageUp=false;
      
     // cSpeakerText.x=cSpeakerText.x+GetViewportX();
      
      
      int d=i+1;//0
      while (d < MaxMessages)
      {
        int from=d;
        int to=d-1;
        Messages[to].Duration=Messages[from].Duration;
        Messages[to].Enabled=Messages[from].Enabled;
        Messages[to].Talking=Messages[from].Talking;
        Messages[to].Text=Messages[from].Text;
        Messages[to].cRoom=Messages[from].cRoom;
        d++;
      }
      return;
      
      
    }
    i++;
  }
  
}


int TeratomaOrificeTimer=800;
int ShootingGalleryTimer=0;

function repeatedly_execute_always()
{
  //return;
  
  if (Pause)
  {
    return;
  }
  Parse();
  
  if (player.Room==9 && gInventory.Visible==false && !ShootingMode && !WonShootingGame)
  {    
    ShootingGalleryTimer++;
    if (ShootingGalleryTimer==1600)
    {
      cMystery.BackgroundSpeech("Step right up, step right up!");
      cMystery.BackgroundSpeech(" ");
      ShootingGalleryTimer=0;
    }
  }
  
  if (player.Room==21)
  {
    TeratomaOrificeTimer++;
    if (TouchedOrifices)
    {
      TouchedOrifices=false;
      TeratomaOrificeTimer=0;
    }
    if (TeratomaOrificeTimer>= 2400 && !TouchedOrifices)
    {
      //if (Game.DoOnceOnly(""))
      //{
        cMystery.BackgroundSpeech("Don't be shy -- she won't bite!");
        TeratomaOrificeTimer=0;
      //}
    }
  }
}

function ClearMessages(int RoomT)
{
  int i=0;
  while (i < MaxMessages)
  {
    if (Messages[i].cRoom==RoomT)
    {
      Messages[i].Enabled=false;
      Messages[i].Text=null; 
      Messages[i].Duration=0;
      Messages[i].cRoom=-1;
    }
    i++;
  }
}

function on_event (EventType event, int data) 
{
  if (event == eEventEnterRoomBeforeFadein)
  {
    cSpeakerText.ChangeRoom(data, cSpeakerText.x, 9000);    
    cSpeakerText.Baseline=9000;
    cSpeakerText.IgnoreLighting=true;
    cSpeakerText.IgnoreWalkbehinds=true;
    
    cSpeakerText.ManualScaling=true;
    cSpeakerText.Scaling=100;        
    int der=8;  
    cSpeakerText.TweenScaling(0.4, 100+der, eLinearTween, eReverseRepeatTween);
    cSpeakerText.LockViewFrame(161, 0, 0);
    
    
     if (data!=21 && data!=9 && data!=6 && data!=13)
  {
    
    cSpeakerText.Transparency=100;
    MessageUp=false;
    return;
  }
    if (player.Room==21)
    {
      TeratomaOrificeTimer=800;
      ClearMessages(21);
      cMystery.BackgroundSpeech("Come one, come all, come close!");
      cMystery.BackgroundSpeech(" ");
      cMystery.BackgroundSpeech("See the magnificent Teratoma!");
      cMystery.BackgroundSpeech(" ");      
      cMystery.BackgroundSpeech("She thrashes! She gnashes! She watches and whispers!");
      cMystery.BackgroundSpeech(" ");
      cMystery.BackgroundSpeech("From thus sprang Eve from Adam's flesh,");
      cMystery.BackgroundSpeech(" ");
      cMystery.BackgroundSpeech("and thus ends Eve in monstrous death.");  
    }
    
    if (player.Room==9)
    {      
      if (Game.DoOnceOnly("EnterRoomBGSpeech9"))
      {     
      ClearMessages(9);
      cMystery.BackgroundSpeech("Step right up, step right up!");
      cMystery.BackgroundSpeech(" ");
      cMystery.BackgroundSpeech("Win yourself a bottle of Nepenthe(tm):");
      cMystery.BackgroundSpeech(" ");
      cMystery.BackgroundSpeech("the brew that puts a bullet in all your bad thoughts!");
      cMystery.BackgroundSpeech(" ");     
      
      }
    }
    if (player.Room==6)
    {      
      if (!FreedDarkThing)
      {
        if (Game.DoOnceOnly("EnterRoomBGSpeech6_Act1"))
        {
          cMystery.BackgroundSpeech("The Ride of the Valkyrie is currently[closed for want of worthy riders.     ");
          cMystery.BackgroundSpeech(" ");
          cMystery.BackgroundSpeech("In the future, the ride will return[should circumstances call for it.");
          cMystery.BackgroundSpeech(" ");
        }
      }
      else 
      {
      
      if (Game.DoOnceOnly("EnterRoomBGSpeech6_Act2"))
      {
        ClearMessages(6);
        cMystery.BackgroundSpeech("The Ride of the Valkyrie is closed again[for want of wings and souls.");
        cMystery.BackgroundSpeech(" ");
        cMystery.BackgroundSpeech("To soar again, bring wings of gold and a hand of souls.");
        cMystery.BackgroundSpeech(" ");
        HeardWings=true;
        dScribe.SetOptionState(17, eOptionOn);
        dScribe.SetOptionState(18, eOptionOn);
        return;
      }
      
      if (!TorchUsedDead && !player.HasInventory(iWings))
      {
        ClearMessages(6);
        cMystery.BackgroundSpeech("The ride can't run without wings and souls.");
      }
      else if (NumSouls<5 && TorchUsedDead)
      {
        ClearMessages(6);
        if (!player.HasInventory(iWings)) cMystery.BackgroundSpeech("The ride can't run without wings and souls.");
        else cMystery.BackgroundSpeech("The ride can't run without more souls.");
        
        if (NumSouls==1) cMystery.BackgroundSpeech("You must bring four more souls to soar again.");
        else if (NumSouls==2) cMystery.BackgroundSpeech("You must bring three more souls to soar again.");
        else if (NumSouls==3) cMystery.BackgroundSpeech("You must bring two more souls to soar again.");
        else if (NumSouls==4) cMystery.BackgroundSpeech("You must bring one more souls to soar again.");
     
      }
      else if (player.HasInventory(iWings) && NumSouls==0)
      {
        ClearMessages(6);
        cMystery.BackgroundSpeech("The ride can't run without souls.");
      }
      else if (!player.HasInventory(iWings) && NumSouls>4)
      {
        ClearMessages(6);
        cMystery.BackgroundSpeech("This ride can't run without wings.");
      }
      
      }
      /*
      if (player.HasInventory(iWings)==false && FreedDarkThing)
      {
        if (Game.DoOnceOnly("EnterRoomBGSpeech6_2"))
        {
          ClearMessages(6);
          cMystery.BackgroundSpeech("The Ride of the Valkyrie is closed again[for want of wings.                                           ");
          cMystery.BackgroundSpeech(" ");
          cMystery.BackgroundSpeech("Return when you are ready to soar.");
          cMystery.BackgroundSpeech(" ");
        }
      }*/
      
    }
    if (player.Room==13)
    {
      //ClearMessages(13);
      
      
    }
    


    
  }
}




