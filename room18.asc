// room script file
bool TalkingCrab=false;
int getLay=0;
function room_Load()
{
  DisableAutosave=true;
  hasnotpressedstart=false;
  gFade.Transparency=0;
  gFade.Visible=true;
  player.Transparency=100;
  SeenHeadcrab=true;
}

function room_Leave()
{
  gFade.Transparency=100;
  gFade.Visible=false;
  player.Transparency=0;
}

function repeatedly_execute_always()
{
  if (getLay==0 && TalkingCrab)
  {
    TalkingCrab=false;
  }
  if (getLay>0)
  {
    getLay--;
  }
  //cEgo.SayBackground(String.Format("%d",getLay));
  if (TalkingCrab)
  {
    if (oCrabmask.Graphic==2238 && getLay>0)
    {
      oCrabmask.SetView(CRABMVIEW, 0, 0);
      oCrabmask.Animate(oCrabmask.Loop, 5, eRepeat, eNoBlock, eForwards);
    }
  }
  else oCrabmask.Graphic=2238;
}

function room_AfterFadeIn()
{
 
 
 
 
 
 
  
 StartCutscene(eSkipESCOnly); 
 gFade.TweenTransparency(1.2, 100, eLinearTween, eBlockTween);
 
 if (visioncount==0) cEgo.Subtitler("   help");
 else if (visioncount==1) cEgo.Subtitler(" help me");
 else if (visioncount>=2) cEgo.Subtitler("help me, please");
 visioncount++;
 
 TalkingCrab=true;
 getLay=SubtitleText.Length*6;
 
 SFX_Play(_RemoveMask, 0);//aRemoveMask.Play();
 oCrabmask.TweenPosition(4.5, oCrabmask.X-90, oCrabmask.Y+55, eEaseInEaseOutTween, eBlockTween); 

  Music_Stop(1000);
 gFade.Transparency=100;
 gFade.Visible=true;
 gFade.TweenTransparency(1.2, 0, eLinearTween, eBlockTween);
 EndCutscene();
 
 cEgo.LockViewFrame(EGOGROAN, 0, 0); 
 cEgo.SetAsPlayer();
}
