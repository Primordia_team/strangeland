// new module header
import function PitchFinder(int getOption);
import function FortuneIntro();
import function MurmurEnd();
import function CardShower(int a, int b, int c);
import function StarfishTalk(String text);
import function DialogKeyboardControls();