// new module script
int StartX[30];
DynamicSprite*getBut[30];
DynamicSprite*DrawShape[30];
bool direction[30];
int directionY[30];
int directionLay[30]; 
int StartXDelay[30];
int drawingSprite[30];



int GetRFromColor(int color)
{
  float floatColor = IntToFloat(color);
  int result = FloatToInt(floatColor / 2048.0) * 8;  
  return result;
}

  
  

function SetButtonMouseOverGraphic(Button*button, int SpeedX, bool Repeat)
{
  if (button.OwningGUI.Visible && button.OwningGUI.Transparency<100)
  {
  
  int graph =button.NormalGraphic;
  bool Pushed=false;
  if (mouse.IsButtonDown(eMouseLeft) && button.Graphic==button.PushedGraphic)
  {
    Pushed=true;
    graph=button.PushedGraphic;
  }
  
  int buttonWidth=Game.SpriteWidth[graph];
  int buttonHeight=Game.SpriteHeight[graph];
  
  int getButtonWidth=button.Width;
  int getButtonHeight=button.Height;
  
  int id=button.ID;
  
  if (getBut[id]!=null)
  {
    getBut[id].Delete();
  }
  getBut[id] = DynamicSprite.Create(buttonWidth, buttonHeight, true);
  
  DrawingSurface*area = getBut[id].GetDrawingSurface();  
  
  
  
  bool Clearer=false;
  if (drawingSprite[id]!=graph)
  {
    Clearer=true;
  }
  drawingSprite[id]=graph;
  if (bmouseover.Graphic==1)//button.Graphic==button.NormalGraphic)
  {
    StartX[id]=buttonHeight;    
    Clearer=true;
    StartXDelay[id]=2;
  }
  
  if (StartX[id]==5)
  {    
    if (Repeat)
    {
      StartX[id]=buttonHeight; 
      Clearer=true;
    }
  }
  
  if (Clearer)
  {
    if (DrawShape[id]!=null)
    {
      DrawShape[id].Delete();
    }
    DrawShape[id] =DynamicSprite.CreateFromExistingSprite(graph, true);//(buttonWidth,buttonHeight); //
  }
  
  DrawingSurface*ar = DrawShape[id].GetDrawingSurface();
  
  
  
  
  int den=5;    
  if (graph==3820 ||graph==3821) den=6;
  if (graph==4696 || graph==4695 || graph==4693 || graph==4694) den=32;
  
  int bWidth=buttonWidth-den;
  if (graph==4696|| graph==4695|| graph==4694|| graph==4693) bWidth=buttonWidth-8;
  int GY=5;
  if (graph==573 ||graph==3821) GY=6;
  if (graph==4696|| graph==4694) GY=6;//20
  if (graph==4695||graph==4693) GY=7;//20
  
  int BHeight=buttonHeight-6;
  if (graph==4696||graph==4695||graph==4693|| graph==4694) BHeight=buttonHeight-8;

  
  int y=StartX[id];
  if (y<GY) y=GY;  
  if (y> BHeight) y=BHeight;
  
  if (StartX[id]==GY)
  {
    //might not be needed!
    ar.Clear();
    ar.DrawingColor=ColorPurple;
    if (graph==3820) 
    {
      ar.DrawRectangle(den,5, buttonWidth-den-1, buttonHeight-7);
    }
    else if (graph==3821) 
    {
      ar.DrawRectangle(den,6, buttonWidth-den-1, BHeight);
    }
    else if (graph==573)
    {
      ar.DrawRectangle(den,6, buttonWidth-den-1, BHeight);      
    }
    else if (graph==4696|| graph==4694)
    {
      ar.DrawRectangle(den,6, bWidth-1, BHeight);
      ar.DrawingColor=COLOR_TRANSPARENT;
      //ar.DrawingColor=15;
      ar.DrawPixel(den, 6);
      ar.DrawPixel(bWidth-1, 6);
      ar.DrawPixel(den, BHeight);            
      ar.DrawTriangle(bWidth-1, BHeight, (bWidth-1), BHeight-3, (bWidth-1)-3, BHeight);
    }
    else if (graph==4695 || graph==4693)
    {
      ar.DrawRectangle(den,GY, bWidth-1, BHeight);
      ar.DrawingColor=COLOR_TRANSPARENT;
      
      ar.DrawPixel(den, GY);
      ar.DrawPixel(bWidth-1, GY);
      ar.DrawPixel(den, BHeight);            
      ar.DrawTriangle(bWidth-1, BHeight, (bWidth-1), BHeight-3, (bWidth-1)-3, BHeight);
    }
    else
    {
      ar.DrawRectangle(den,5, buttonWidth-den-1, BHeight);
    }
  }
  

  
  StartX[id]=y;
    

  int n=den;
  while (n< bWidth)
  {    
    
    if (y==StartX[id])
    {
      directionLay[id]++;
      
      int limit=3;
      int bylimit=2;
      if (graph==3820 || graph==4694 || graph==4693)
      {
        limit=3;
        bylimit=1;
      }
      if (graph==339)
      {
        limit=6;
        bylimit=2;
      }
      if ( graph==4695 || graph==4696)
      {
        limit=8;
        bylimit=1;
      }
      
      if (directionLay[id] >=limit)
      {
        directionLay[id]=0;
        if (!direction[id])
        {
          directionY[id]++;
          if (directionY[id]>bylimit)
          {
            directionY[id]=bylimit;
            direction[id]=!direction[id];
          }      
        }
        else 
        {
          directionY[id]--;
          if (directionY[id]<-bylimit)
          {
            directionY[id]=-bylimit;
            direction[id]=!direction[id];
          }
        }
      }
    
    int getColor=ar.GetPixel(n,y); //+directionY[id]
    
    bool cond =false;
    if (graph==4696 || graph==4694) cond =GetRFromColor(getColor)>18;//||graph==4695
    else if (graph==4695|| graph==4693) cond=true;
    else cond = GetRFromColor(getColor)<18;
    
    if (cond)//
    {
      ar.DrawingColor=ColorPurple;
      
      int setR=y+directionY[id];
      
      if (setR<GY) setR=GY;
      if (graph==573) 
      {
       // if (setR<GY+1) setR=GY+1;
      }
      
      
      if (setR> BHeight) setR=BHeight;
      
      
      
     // ar.DrawPixel(n, setR);
      
      int g=setR;
      
      int f=BHeight+1;//buttonHeight-5;
      if (graph==3820) f=BHeight;
      //if (graph==573) f=buttonHeight-6;
      
      
      while (g < f)
      {
        
        if (g<GY) g=GY;
        if (g> BHeight) g=BHeight;
        
        if (graph==4696 || graph==4695 || graph==4693|| graph==4694)
        {          
          
          if ( (n==den && g==GY) || (n==den && g==BHeight) 
          || (n==bWidth-1 && g==GY) )
          {            
          }
          else 
          {
            ar.DrawPixel(n, g);
          }
          ar.DrawingColor=COLOR_TRANSPARENT;          
          ar.DrawTriangle(bWidth-1, BHeight, (bWidth-1), BHeight-3, (bWidth-1)-3, BHeight);
          ar.DrawingColor=ColorPurple;
        }
        else 
        {
          ar.DrawPixel(n, g);
        }
        g++;
      }
      
    }
    }
    
    n++;
  }
  
  ar.DrawingColor=button.TextColor;
  
  
  ar.Release();
  area.DrawImage(0, 0, DrawShape[id].Graphic);
  //button.Text="";
  
  //area.DrawStringWrapped(0, 2, button.Width, button.Font, eAlignCentre, button.Text);
  if (Pushed)
  {
    area.DrawingColor=16;
    if (graph==4694)area.DrawStringWrapped(0, 3, button.Width, button.Font, eAlignCentre, button.Text);
    else area.DrawStringWrapped(0, 2, button.Width, button.Font, eAlignCentre, button.Text);
    area.DrawingColor=button.TextColor;
    area.DrawStringWrapped(1, 3, button.Width, button.Font, eAlignCentre, button.Text);
  }
  else 
  {
    area.DrawingColor=button.TextColor;
    if (graph==4694)area.DrawStringWrapped(0, 3, button.Width, button.Font, eAlignCentre, button.Text);
    else area.DrawStringWrapped(0, 2, button.Width, button.Font, eAlignCentre, button.Text);
  }
  
  StartXDelay[id]++;
  if (StartXDelay[id]>1)
  {
    StartX[id]-=1;//SpeedX;
    StartXDelay[id]=0;
  }
  
 
  
  area.Release();
  
  //change to mouseover
 // button.MouseOverGraphic=getBut[id].Graphic;
  
  //button.Height=getButtonHeight;
 // button.Width=getButtonWidth;
  
   bmouseover.SetPosition(button.X, button.Y);
   bmouseover.SetSize(getButtonWidth, getButtonHeight);
   gMouseover.Transparency=button.OwningGUI.Transparency;
   gMouseover.SetPosition(button.OwningGUI.X, button.OwningGUI.Y);
   gMouseover.ZOrder=button.OwningGUI.ZOrder+1;
   
   bmouseover.Text="";//button.Text;
   bmouseover.TextColor=button.TextColor;
   bmouseover.Font=button.Font;
   
   bmouseover.NormalGraphic=getBut[id].Graphic;
   gMouseover.Visible=true;
  }
  
}
bool heldOverSlider=false;
GUIControl*ControlGrabbed;

function SetValuesFromOptions()
{
  System.Volume=sldMasterVolume.Value;
  int setSp=FloatToInt(255.0*(IntToFloat(sldVoiceVolume.Value)/100.0), eRoundNearest);
  if (setSp<0) setSp=0;
  if (setSp>255) setSp=255;
  
  SetSpeechVolume(setSp);
  
  if (System.SupportsGammaControl) 
  {
    System.Gamma = sldBrightness.Value;
  }
  Mouse.Speed=IntToFloat(sldMouseSpeed.Value);
  if (Mouse.Speed<0.1) Mouse.Speed=0.1;
  
  //Game.SetAudioTypeVolume(eAudioTypeSound, sldSound.Value, eVolExistingAndFuture);
  //Game.SetAudioTypeVolume(eAudioTypeAmbientSound, sldSound.Value, eVolExistingAndFuture);
  //SFX
  //SFX_ForceGlobalVolume(sldSound.Value);
  
 // MVolume=sldMusic.Value;
 // Game.SetAudioTypeVolume(eAudioTypeMusic, sldMusic.Value, eVolExistingAndFuture);
}


function SaveSettings()
{
  /*
  sldMasterVolume.Value
  sldMusic.Value
  sldSound.Value
  sldVoiceVolume.Value
  sldBrightness.Value
  sldMouseSpeed.Value*/
  
  
  File *output = File.Open("$SAVEGAMEDIR$/settings.ini", eFileWrite);
  //output.WriteRawLine("There was an error playing sound1.wav");
  
  output.WriteRawLine(String.Format("%d",sldMasterVolume.Value));
  output.WriteRawLine(String.Format("%d",sldMusic.Value));
  output.WriteRawLine(String.Format("%d",sldSound.Value));
  output.WriteRawLine(String.Format("%d",sldVoiceVolume.Value));
  output.WriteRawLine(String.Format("%d",sldBrightness.Value));
  output.WriteRawLine(String.Format("%d",sldMouseSpeed.Value));
  output.Close();
  
  
}

function RestoreSettings()
{
  /*
  sldMasterVolume.Value
  sldMusic.Value
  sldSound.Value
  sldVoiceVolume.Value
  sldBrightness.Value
  sldMouseSpeed.Value*/
  
  
  File *input = File.Open("$SAVEGAMEDIR$/settings.ini", eFileRead);
  //output.WriteRawLine("There was an error playing sound1.wav");
  
  int Valuer[10];
  int i=0;
  if (input != null) 
  {
    while (i<6) 
    {
      String line = input.ReadRawLineBack();

      Valuer[i]=line.AsInt;
      i++;
      
    }
    input.Close();
  }
  
if (System.Windowed)
    {
      bwindowed.NormalGraphic=4695;
      bfullscreen.NormalGraphic=4696;
    }
    else
    {
      bwindowed.NormalGraphic=4696;
      bfullscreen.NormalGraphic=4695;
    }
    
  sldMasterVolume.Value=Valuer[0];  
  sldMusic.Value=Valuer[1];
  sldSound.Value=Valuer[2];
  sldVoiceVolume.Value=Valuer[3];
  sldBrightness.Value=Valuer[4];
  sldMouseSpeed.Value=Valuer[5];
  
  SetValuesFromOptions();
    
}

function Defaults()
{
  sldMasterVolume.Value=100;  
  sldMusic.Value=100;
  sldSound.Value=100;
  sldVoiceVolume.Value=50;
  sldBrightness.Value=100;
  sldMouseSpeed.Value=1;
  
  SetValuesFromOptions();
  
  SaveSettings();
}



function OptionsHandle()
{ 
  
  if (gOptions.Visible)
  {
    GUIControl*grabControl=GUIControl.GetAtScreenXY(mouse.x, mouse.y);
    String MouseSpeedText;
    String MusicVolumeText;    
    String SoundVolumeText;
    String MasterVolumeText;
    String SpeechVolumeText;
    String BrightnessText;
    
    MouseSpeedText="MOUSE SPEED";
    MusicVolumeText="MUSIC VOLUME";
    SoundVolumeText="SFX VOLUME";
    MasterVolumeText="MASTER VOLUME";
    SpeechVolumeText="VOICE VOLUME";
    BrightnessText="BRIGHTNESS";
    
    
    if (System.Windowed)
    {
      bwindowed.NormalGraphic=4695;
      bfullscreen.NormalGraphic=4696;
    }
    else
    {
      bwindowed.NormalGraphic=4696;
      bfullscreen.NormalGraphic=4695;
    }
    
    
    if (mouse.IsButtonDown(eMouseLeft) && grabControl!=null && grabControl.AsSlider!=null && !heldOverSlider)
    {
      heldOverSlider=true;
      ControlGrabbed=grabControl;     
    }
    if (mouse.IsButtonDown(eMouseLeft) && heldOverSlider)
    {
      Label*toAdjust;
      if (ControlGrabbed.AsSlider==sldMouseSpeed) toAdjust=lblMouseSpeed;
      else if (ControlGrabbed.AsSlider==sldMusic)toAdjust=lblMusicVol;
      else if (ControlGrabbed.AsSlider==sldSound)toAdjust=lblSoundVol;
      else if (ControlGrabbed.AsSlider==sldMasterVolume)toAdjust=lblMasterVolume;
      else if (ControlGrabbed.AsSlider==sldVoiceVolume)toAdjust=lblVoiceVolume;
      else if (ControlGrabbed.AsSlider==sldBrightness)toAdjust=lblBrightness;
      
      if (toAdjust!=null)
      {
        
        String en="";
        
        if (toAdjust==lblMouseSpeed) en=String.Format("%d",sldMouseSpeed.Value);
        if (toAdjust==lblMusicVol) en=String.Format("%d",sldMusic.Value);
        if (toAdjust==lblSoundVol) en=String.Format("%d",sldSound.Value);
        if (toAdjust==lblMasterVolume) en=String.Format("%d",sldMasterVolume.Value);
        if (toAdjust==lblVoiceVolume) en=String.Format("%d",sldVoiceVolume.Value);
        if (toAdjust==lblBrightness) en=String.Format("%d",sldBrightness.Value);
        
        lblOptVal.Text=en;
        
        float sum = (IntToFloat(ControlGrabbed.AsSlider.Width)/IntToFloat(ControlGrabbed.AsSlider.Max));
        int vx=ControlGrabbed.AsSlider.X+FloatToInt((sum*IntToFloat(ControlGrabbed.AsSlider.Value)), eRoundNearest)+2;
        
        //lblOptVal.Text=String.Format("%f",sum);
        lblOptVal.SetPosition(vx,ControlGrabbed.AsSlider.Y-11); 
        
      }
    }    
    
    if (!mouse.IsButtonDown(eMouseLeft))
    {
      heldOverSlider=false;
      
      lblMouseSpeed.Text=MouseSpeedText;
      lblMusicVol.Text=MusicVolumeText;
      lblSoundVol.Text=SoundVolumeText;
      lblMasterVolume.Text=MasterVolumeText;
      lblVoiceVolume.Text=SpeechVolumeText;
      lblBrightness.Text=BrightnessText;
      lblOptVal.Text="";
      
    }
    
  }
}

function repeatedly_execute()
{
  OptionsHandle();
}
function game_start()
{
  if (!File.Exists("$SAVEGAMEDIR$/settings.ini"))
  {    
    Defaults();
    SaveSettings();
  }
  else 
  {
    RestoreSettings();
  }
}

function on_event (EventType event, int data) 
{
  if (event == eEventRestoreGame )
  {
    RestoreSettings();
  }
}


function repeatedly_execute_always()
{
  int Global_StartX=0;
  int Global_Speed=8;
  int Global_Repeat=false;
  
  GUIControl*grabControl=GUIControl.GetAtScreenXY(mouse.x, mouse.y);
  
  if (grabControl==boptback || grabControl==bfullscreen|| grabControl==bwindowed
  || grabControl==bSaveUIback || grabControl==bsaveUIerase || grabControl==bSaveUIsave
  || grabControl==bquitUIYes || grabControl==bquitUINo|| grabControl==boptdefaults)
  {
    SetButtonMouseOverGraphic(grabControl.AsButton, Global_Speed, Global_Repeat);
  }
  else 
  {
    bmouseover.NormalGraphic=1;
    gMouseover.Visible=false;
  }
  
  /*
  
  SetButtonMouseOverGraphic(boptback, Global_Speed, Global_Repeat);
  SetButtonMouseOverGraphic(bfullscreen, Global_Speed+3, Global_Repeat);
  SetButtonMouseOverGraphic(bwindowed, Global_Speed, Global_Repeat);
  
  SetButtonMouseOverGraphic(bSaveUIback, Global_Speed, Global_Repeat);
  SetButtonMouseOverGraphic(bsaveUIerase, Global_Speed+3, Global_Repeat);
  SetButtonMouseOverGraphic(bSaveUIsave, Global_Speed, Global_Repeat);
  
  SetButtonMouseOverGraphic(bquitUIYes, Global_Speed, Global_Repeat);
  SetButtonMouseOverGraphic(bquitUINo, Global_Speed, Global_Repeat);
  
  SetButtonMouseOverGraphic(boptdefaults, Global_Speed, Global_Repeat);
  */
}
