// new module header
import function DisableHint(int id);
import function EnableHint(int id);
import function DoveOptionScribe();
import function DepositCoin();
import function CallNumber(int num, bool dontadd);
import function CallDial(int numberx, bool fromkeypress=false);
import function SuperEgo(int value);
import function RunCoaster();
import function BlackDogCloseUP(bool spike, bool attack);
import function PhoneCallCloseUp(bool start);
import function HoneCloseUp();