// room script file
int gTr;


function room_AfterFadeIn()
{
  PlayClip(aPurgatory1a);
  StartCutscene(eSkipAnyKey);
    
  int dd=gWSLogo.BackgroundGraphic;
  while (dd <= 1351)
  {
    if (dd==1351)
    {
      SFX_Play(_WWShine1a, 0);//aWWSshine1a.Play();
    }
    gWSLogo.BackgroundGraphic=dd;
    WaitC(3);
    dd++;
  }
  
  dd=1843;
  gWSLogo.BackgroundGraphic=dd;
  WaitC(3);
  while (dd <= 1846)
  {
    gWSLogo.BackgroundGraphic=dd;
    WaitC(3);
    dd++;
  }
  WaitC(40);
  gWSLogo.ZOrder=gFade.ZOrder-1;
  //gFade.Visible=true;
  gWSLogo.TweenTransparency(1.5, 100, eEaseOutTween, eBlockTween);
  
  gWSLogo.BackgroundGraphic=1848;
  WormwoodLogo=false;
  gWSLogo.TweenTransparency(1.5, 0, eEaseOutTween, eBlockTween);
  
  WaitC(60);
  TweenViewportY(5.0, 0, eEaseOutTween, eNoBlockTween);
  gWSLogo.TweenTransparency(1.5, 100, eEaseOutTween, eBlockTween);
  gWSLogo.Visible=false;
  
  oblack.SetView(CLLIGHT, 0, 0);
  oblack.Baseline=13;
  oblack.Transparency=100; 
  oblack.Y=360;
  //EndCutscene();
  
  //StartCutscene(eSkipAnyKey);
  
    //ADDED CROWS TO INTRO HERE
  oIcrows.SetView(45);
  oIcrows.Animate(3, 3, eRepeat, eNoBlock, eForwards);
  oIcrows.TweenPosition(4.5, -120, oIcrows.Y-70, eLinearTween, eNoBlockTween);
  WaitC(60);
  WaitC(5*40);
  //oblack.SetView(CLLIGHT, 0, 0);
  oblack.Transparency=0;
  oblack.Baseline=13;
  
  
  
  oblack2.SetView(CLLFACE, 0, 0);
  oblack2.Visible=true;
  oblack2.Baseline=16;
  SFX_Play(_flashtwo1a, 0);
  //aFlashTwo1a.Play();
  oblack2.Animate(0, 3, eOnce, eNoBlock, eForwards);
  oblack.Animate(0, 3, eOnce, eNoBlock, eForwards);
  while (oblack.Animating)
  {
    if (oblack.Frame == 4)
    {
      SFX_Play(_flashsix1a, 0);//aflashsix
    }
    WaitC(1);
  }
  WaitC(2);
  oEgo.Animate(0, 1, eOnce, eBlock, eForwards);
  WaitC(80);
  
  gWSLogo.Transparency=100;
  gWSLogo.Visible=true;
  gWSLogo.BackgroundGraphic=1847;
  gWSLogo.TweenTransparency(1.5, 0, eEaseOutTween, eBlockTween);
  WaitC(80);
  gWSLogo.TweenTransparency(1.5, 100, eEaseOutTween, eBlockTween);
  
  
  gFade.TweenTransparency(1.5, 0, eEaseInEaseOutTween, eBlockTween);
  cEgo.Transparency=gTr;
  cEgo.ChangeRoom(1);
  //EndCutscene();
  
}

DynamicSprite*blacker;

function room_Load()
{
  mouse.Visible=false;
  blacker = DynamicSprite.Create(Room.Width, Room.Height, true);
  DrawingSurface*area = blacker.GetDrawingSurface();
  area.Clear(16);
  area.Release();
  oblack.Graphic=blacker.Graphic;
  gTr=cEgo.Transparency;
  cEgo.Transparency=100;
  gFade.Transparency=100;
  gFade.Visible=false;
  gWSLogo.Visible=true;
  oEgo.SetView(EGOCLOSEUP, 0, 0);
}


function repeatedly_execute_always()
{  
  
  
  ParallaxUpdate();
}
  