// new module header
import function Abs(int value);
import function FaceRotate(this Character*,int dx, int dy, bool block=false);
import function PickUp(this Character*);
import function WaitC(int time);
import function CreateBlur();
import function SetWalkBehindBase_New(int id, int base);
import function FakeScreen(int ids=0);