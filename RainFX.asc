// new module script

DynamicSprite*raingraph[2];
DynamicSprite*rainpath;
Overlay*MainRain;
Overlay*MainRainFG;
AudioClip *MusicVid;

#define INVENTORY_ITEMS 34
DynamicSprite*itemgraph[INVENTORY_ITEMS];
DynamicSprite*ActualInvGraphic[INVENTORY_ITEMS];
int InvGraphs[];
int InvTorchGraphs[7];
DynamicSprite*itemTorchgraph[7];
DynamicSprite*ActualTorchInvGraphic[7];

Overlay*Subtitle;

function SetMurmurProphecy(bool setTo)
{
  ViewFrame*er=Game.GetViewFrame(MURMURTALKV,0, 0);
  MurmurHasBeenTinted=false;
  bportrait.NormalGraphic=er.Graphic;
  MurmurProphecy=setTo;
  if (setTo)
  {    
    SFX_Stop(_Mumurtalk1a, 3000);////aMurmurtalk1a.Stop();
  }
  else 
  {
    SFX_Play(_Mumurtalk1a, 0);//
    SFX_SetVolume(_Mumurtalk1a, 0);
    
    while  (SFX_GetVolume(_Mumurtalk1a)+3<sldSound.Value) 
    {
      SFX_SetVolume(_Mumurtalk1a,SFX_GetVolume(_Mumurtalk1a)+3);
      //grabChan.Volume+=3;
      WaitC(1);
    }
    
    /*
    aMurmurtalk1a.Play(eAudioPriorityVeryHigh);
    int d=0;
    while (d < System.AudioChannelCount)
    {
      AudioChannel*grabChan = System.AudioChannels[d];
      if (grabChan.IsPlaying)
      {
        if (grabChan.PlayingClip == aMurmurtalk1a)
        {
          grabChan.Volume=0;
        }
      }
      d++;
    }
    d=0;
    while (d < System.AudioChannelCount)
    {
      AudioChannel*grabChan = System.AudioChannels[d];
      if (grabChan.IsPlaying)
      {
        if (grabChan.PlayingClip == aMurmurtalk1a)
        {
          while  (grabChan.Volume+3<sldSound.Value) 
          {
            grabChan.Volume+=3;
            WaitC(1);
          }
        }
      }
      d++;
    }*/
  }
  
  
  int firstspriter = 1356;
  int getView = MURMURTALKV;
  
  if (setTo)
  {
    SFX_Play(_Prophecy1a, 0);//aProphecy1a.Play(eAudioPriorityVeryHigh, eOnce);
    firstspriter = 4353;
  }
  
  
  if (setTo)
  {
    if (gMurmurProph.Transparency==100)
    {
      gMurmurProph.Transparency=0;
      gMurmurProph.ZOrder=gPortraits.ZOrder+1;
      ViewFrame*getVFrame=Game.GetViewFrame(MURMURTALKV, 0, 0);
      bmurmur.NormalGraphic=getVFrame.Graphic;
      gMurmurProph.TweenTransparency(0.35, 100, eEaseInTween, eNoBlockTween);
    }
  }
  
   // getVFrame;
      
    
    
    int i=0;  
    while (i < Game.GetFrameCountForLoop(getView, 0))
    {
      ViewFrame*getVFrame=Game.GetViewFrame(getView, 0, i);
      
      int setV = firstspriter + i;
      if (setV == 1371)
      {
        firstspriter+=1;
        setV=1372;
      }
      
      getVFrame.Graphic= setV;
      getVFrame=Game.GetViewFrame(getView, 1, i);
      getVFrame.Graphic= setV;
      getVFrame=Game.GetViewFrame(getView, 2, i);
      getVFrame.Graphic= setV;
      getVFrame=Game.GetViewFrame(getView, 3, i);
      getVFrame.Graphic= setV;
      
      i++;
    }
    
  if (setTo==false)
  {
   gMurmurProph.TweenTransparency(0.35, 0, eEaseInTween, eBlockTween);
   gMurmurProph.Transparency=100;  
  } 
    
  
}
AudioClip* GetClip()
{
  return MusicVid;
}


function Subtitler(this Character*,String text, int color)
{
  int colr=color;
  int sx=640/2;
  int gwidth=GetTextWidth(text, Game.SpeechFont);
  sx-=(gwidth/2);
  int sy=320;
  if (player.Room==20)
  {
    sx=(cWoman.x-(gwidth/2))-GetViewportX()-8;
    sy=27;
  }
  if (player.Room==13)
  {
    sy-=30;
  }
  Subtitle = Overlay.CreateTextual(sx, sy, 640, Game.SpeechFont, colr, text);
  subdelay=text.Length*2;
  SubtitleText=text;
  if (player.Room==13)
  {
   //
    cSlideEgo.Animate(cSlideEgo.Loop, 3, eRepeat, eNoBlock, eForwards);
    WaitMouseKey((text.Length*2));    
    cSlideEgo.LockViewFrame(cSlideEgo.View, cSlideEgo.Loop, 0);
    //WaitC(80);
   // if (Subtitle!=null && Subtitle.Valid)
   // {
   //   Subtitle.Remove();
   // }
  //  subdelay=0;
    //subdelay=;    
  }
  else 
  {
    if (player.Room==20) subdelay=50;
    else subdelay=110;
  }
}

function RemoveSubtitle()
{
  if (subdelay>0)
  {
    subdelay--;
    if (player.Room==13)
    {
      
    }
    
  }
  else 
  {
    if (cSlideEgo.Animating==false && Subtitle!=null && Subtitle.Valid)
    {
      Subtitle.Remove();
      subdelay=0;
      
      
    }
  }
}



function Abs(int value)
{
  if (value <0) value = (-1)*value;
  return value;
}


function EgoCoverEars(float delay, bool yell)
{
  while (  (gBlack.Transparency<100 && gBlack.Visible) || (gFade.Transparency<100 && gFade.Visible))
  {
    WaitC(1);
  }
  
  
  int getTra = cEgo.Transparency;
      
  cRenderScreech.Transparency=100;
  ScreechOccuring=true;
  WaitC(4);
  RenderScreechAnim();
 
  
  cRenderScreech.TweenTransparency(2.3,70, eLinearTween, eNoBlockTween);
  
  
  int getLoop = cEgo.Loop;
  
  if (cEgo.Room==7)
  {
    dxv=3;
    if (CoverEarsFace)
    {
      CoverEarsFace=false;
      dxv=0;
    }
  }
  
  cEgo.LockViewFrame(EGOLA, 0, 0);
  cEgo.Animate(cEgo.Loop, 3, eOnce, eBlock, eForwards);
  
  cEgo.LockViewFrame(EGOCOVER, dxv, 0);
  WaitC(20);
  bool one_letter=false;
  String yellText="Aaagghhh!";
  int yellFrame=0;
  String yellTextToDisplay="";
  int yellFrameDelay=0;
  
  cEgo.Animate(cEgo.Loop, 3, eOnce, eNoBlock, eForwards);
  while (cEgo.Animating)
  {
    if (cEgo.Frame==5 && yell)
    {
      one_letter=true;
    }
    if (one_letter)
    {
      yellFrameDelay++;
      if (yellFrameDelay>2)
      {
        yellTextToDisplay=yellTextToDisplay.Append(String.Format("%c",yellText.Chars[yellFrame]));
        cEgo.SayBackground(yellTextToDisplay);
        yellFrame++;
        yellFrameDelay=0;
      }
    }
    
    WaitC(1);
  }
  
  cEgo.LockViewFrame(EGOCOVER, dxv+1, 0);
  int delayx = FloatToInt(delay*37.0, eRoundNearest);//40 //45
  //cEgo.SayBackground(String.Format("%d",delayx));
  WaitC(delayx);
  cEgo.LockViewFrame(EGOCOVER, dxv+2, 0);
  cEgo.Animate(cEgo.Loop, 3, eOnce, eBlock, eForwards);
  
  //cEgo.LockViewFrame(EGOLA, 0, 0);
  //cEgo.Animate(cEgo.Loop, 3, eOnce, eBlock, eForwards);
  
  cEgo.LockViewFrame(EGOGRIM, 0, 0);
  cEgo.Animate(cEgo.Loop, 3, eOnce, eBlock, eForwards);
  
  cEgo.UnlockView();
  cEgo.FaceDirection(eDirectionDownLeft, eBlock);
  //cEgo.Loop=getLoop;
  
  
  cRenderScreech.TweenTransparency(1.8,100, eLinearTween, eNoBlockTween);

  ScreechOccuring=false;
  
}







function SetInvGraphic(int id, bool RestoreState)
{
  //return;
  //restore all graphics for invs
  
  int ColorForInvs=ColorPurple;//23279;
  int def;
  
  
  if (!RestoreState)
  {
  
  def=id;
  if (id==iTorch.ID)
  {    
    //InvGraphs[iTorch.ID]=itemTorchgraph[GrabTorchGraphic-3858].Graphic;//3858;//replace with graph
    inventory[id].Graphic=itemTorchgraph[GrabTorchGraphic-3858].Graphic;//3858;//replace with 
    return;
  }
  else if (id==iHoneddagger.ID)
  {
    InvGraphs[iHoneddagger.ID]=2431;//replace with graph
  }
  
  if (Game.DoOnceOnly(String.Format("CreateInvOutGraphic%d",def)))
  {
  int by=2;
  itemgraph[id]=DynamicSprite.Create(Game.SpriteHeight[InvGraphs[id]]+by, Game.SpriteWidth[InvGraphs[id]]+by, true);
  
  DrawingSurface*area = itemgraph[id].GetDrawingSurface();

  area.DrawImage(by/2, by/2, InvGraphs[id]);
  area.DrawImage((by/2)+1, (by/2)+1, InvGraphs[id]);
  area.DrawImage((by/2)+1, (by/2)-1, InvGraphs[id]);
  area.DrawImage((by/2)+1, (by/2), InvGraphs[id]);
  
  area.DrawImage((by/2)-1, (by/2)+1, InvGraphs[id]);  
  area.DrawImage((by/2)-1, (by/2)-1, InvGraphs[id]);
  area.DrawImage((by/2)-1, (by/2), InvGraphs[id]); 
  
  area.DrawImage((by/2), (by/2)+1, InvGraphs[id]);  
  area.DrawImage((by/2), (by/2)-1, InvGraphs[id]);
  
  area.DrawingColor=ColorForInvs;
  
  int v=0;
 
  while (v < itemgraph[id].Height)
  {
     int h =0;
     while (h <itemgraph[id].Width)
     {
       int grabPix=area.GetPixel(v, h);
       if (grabPix!=-1)
       {
         area.DrawPixel(v, h);         
       }
       h++;
     }     
    v++;
  }
  
  
  
  
  area.DrawImage(by/2, by/2, InvGraphs[id]);
  //area.DrawImage((by/2), (by/2), InvGraphs[id]);
  area.Release();
  }
  inventory[id].Graphic=itemgraph[id].Graphic;
  
  }
  else 
  {
    if (inventory[id]==iTorch)
    {
      inventory[id].Graphic=GrabTorchGraphic;//InvGraphs[id];
    }
    else inventory[id].Graphic=ActualInvGraphic[id].Graphic;
    
    
    
  }
  
}

function RestoreInvs()
{
  //return;
  int d=1;
      while (d <= Game.InventoryItemCount)
      {
        SetInvGraphic(d, true);
        d++;
      }  
}



function game_start()
{
  cSeestar.SpeechColor=ColorPurple;
    cChar1.Transparency=100;
  cChar2.Transparency=100;
  cChar3.Transparency=100;
  cSlideBG.LockViewFrame(SLIDETALK, 1, 0);
  cSlideFront.LockViewFrame(SLIDETALK, 2, 0);
  cSlideEgo.LockViewFrame(SLIDETALK, 0, 0);
  
  cSlideBG.ChangeRoom(-1);
   cSlideFront.ChangeRoom(-1);
    cSlideEgo.ChangeRoom(-1);
  
  gTeratoma.Visible=false;
  gKnot.Visible=false;
  gKnot.Transparency=100;
  cDTSin1.Transparency=100;
  cDTSin2.Transparency=100;
  cDTSin3.Transparency=100;
  
  
  
  InvGraphs=new int[INVENTORY_ITEMS+1];
  
  
  int dx=0;
  while (dx < 7)
  {
    InvTorchGraphs[dx]=iTorch.Graphic+dx;
  if (Game.DoOnceOnly(String.Format("CreateInvTorchBigGraphic%d",dx)))
  {
  int by=2;
  itemTorchgraph[dx]=DynamicSprite.Create(Game.SpriteHeight[InvTorchGraphs[dx]]+by, Game.SpriteWidth[InvTorchGraphs[dx]]+by, true);
  
  DrawingSurface*area = itemTorchgraph[dx].GetDrawingSurface();

  area.DrawImage(by/2, by/2, InvTorchGraphs[dx]);
  area.DrawImage((by/2)+1, (by/2)+1, InvTorchGraphs[dx]);
  area.DrawImage((by/2)+1, (by/2)-1, InvTorchGraphs[dx]);
  area.DrawImage((by/2)+1, (by/2), InvTorchGraphs[dx]);
  
  area.DrawImage((by/2)-1, (by/2)+1, InvTorchGraphs[dx]);  
  area.DrawImage((by/2)-1, (by/2)-1, InvTorchGraphs[dx]);
  area.DrawImage((by/2)-1, (by/2), InvTorchGraphs[dx]); 
  
  area.DrawImage((by/2), (by/2)+1, InvTorchGraphs[dx]);  
  area.DrawImage((by/2), (by/2)-1, InvTorchGraphs[dx]);
  
  area.DrawingColor=ColorPurple;
  
  int v=0;
 
  while (v < itemTorchgraph[dx].Height)
  {
     int h =0;
     while (h <itemTorchgraph[dx].Width)
     {
       int grabPix=area.GetPixel(v, h);
       if (grabPix!=-1)
       {
         area.DrawPixel(v, h);         
       }
       h++;
     }     
    v++;
  }
  
  
  
  
  area.DrawImage(by/2, by/2, InvTorchGraphs[dx]);
  area.Release();
  }
  dx++;
  }
  
  int d=1;
  while (d <= Game.InventoryItemCount)
  {
    InvGraphs[d]=inventory[d].Graphic;
    int by=2;
    ActualInvGraphic[d]=DynamicSprite.Create( Game.SpriteHeight[InvGraphs[d]]+by, Game.SpriteWidth[InvGraphs[d]]+by, true );
    DrawingSurface*area = ActualInvGraphic[d].GetDrawingSurface();
    area.DrawImage(by/2, by/2, InvGraphs[d]);
    area.Release();    
    
    //cEgo.AddInventory(inventory[d]);
    SetInvGraphic(d, false);
    SetInvGraphic(d, true);
    d++;
  }
  
  
  
  
  
  
}



struct RainSplashParticle
{
  int x;
  int y;
  int life;
  int graphic;
  int graphlay;
  int transparency;
};

struct RainDrip
{
  int x;
  int y;
  int baseline;
  int loop;
  int animdelay;
  int room;
};

RainDrip RainDrips[20];
int currentraindrip=0;


function RainFXAddDrip(int x,int y, int Baseline, int type, int AnimationDelay)
{  
  int id=13+currentraindrip;
  Character*RainMan = character[id];
  RainMan.x=x;
  RainMan.y=y;
  RainMan.Baseline=Baseline;
  RainMan.LockViewFrame(RAINONE, type, 0);
  RainMan.Animate(RainMan.Loop, AnimationDelay, eRepeat, eNoBlock, eForwards);
  RainMan.ChangeRoom(player.Room);  
  currentraindrip++;
}



RainSplashParticle RainSplashParticles[10000];

int currentrains=0;
int maxrains=0;
int updatedelay=0;
int rainpathgraph=0;
int updatinvarl=0;


int MainRainDelay=0;
int MainRainDelayFrame=625;
int MainRainDelayFrame2=636;


int RainLightDelay[4];
int RainLightFrame[4];

function RainUpdateLight(int Animation_Delay, Object*farain, Object*midrain, Object*closerain)
{
  if (Game.DoOnceOnly("SetUpLightRain"))
  {
    RainLightFrame[0]=1017;
    RainLightFrame[1]=1036;
    RainLightFrame[2]=636;
  }
  
  
  
  if (Game.DoOnceOnly(String.Format("SetUpLightRain%d",player.Room)))
  {
    farain.Clickable=false;
    midrain.Clickable=false;
    closerain.Clickable=false; 
    closerain.Transparency=30; 
    farain.Baseline=1001;
    midrain.Baseline=1002;
    closerain.Baseline=1003;  
    farain.X=0;farain.Y=360;
    midrain.X=0;midrain.Y=360;
    closerain.X=0;closerain.Y=360;
  }
  
  RainLightDelay[0]++;
  if (RainLightDelay[0]>Animation_Delay)
  {
    RainLightDelay[0]=0;
    farain.Graphic=RainLightFrame[0];    
    RainLightFrame[0]++;
    if (RainLightFrame[0]>=1035)
    {
      RainLightFrame[0]=1017;
    }
  }
  RainLightDelay[1]++;
  if (RainLightDelay[1]>Animation_Delay)
  {
    RainLightDelay[1]=0;
    midrain.Graphic=RainLightFrame[1];    
    RainLightFrame[1]++;
    if (RainLightFrame[1]>=1054)
    {
      RainLightFrame[1]=1036;
    }
  }
  RainLightDelay[2]++;
  if (RainLightDelay[2]>Animation_Delay)
  {
    RainLightDelay[2]=0;
    closerain.Graphic=RainLightFrame[2];
    RainLightFrame[2]++;
    if (RainLightFrame[2]>=650)
    {
      RainLightFrame[2]=636;
    }
  }
}



function RainUpdate(int Animation_Delay)
{
  MainRainDelay++;
  if (MainRainDelay>Animation_Delay)
  {
    MainRainDelay=0;
    MainRain = Overlay.CreateGraphical(0-GetViewportX(), 0, MainRainDelayFrame, true);
    
    MainRainDelayFrame++;
    if (MainRainDelayFrame>=636)
    {
      MainRainDelayFrame=625;
    }
  }
  
  MainRainFG = Overlay.CreateGraphical(0-GetViewportX(), 0, MainRainDelayFrame2, true);
  MainRainDelayFrame2++;
  if (MainRainDelayFrame2>=650)
  {
    MainRainDelayFrame2=636;
  }
  
  if (MainRain!=null && MainRain.Valid)
  {
    MainRain.X=-GetViewportX();
  }
  if (MainRainFG!=null && MainRainFG.Valid)
  {
    MainRainFG.X=-GetViewportX();
  }
  
}


function CreateRainSplashParticle(int RW, int RH)
{
  //int RH=Room.Height;
  //int RW=Room.Width;  
  
  int ytoB=Random(RH);
  int xtoB=Random(RW);
  
  if (rainpath!=null) rainpath.Delete();
  rainpath=DynamicSprite.CreateFromExistingSprite(rainpathgraph, true);
  DrawingSurface*getPath = rainpath.GetDrawingSurface();
  int grabPixel=getPath.GetPixel(xtoB, ytoB);
  while (grabPixel== -1 || grabPixel==65535)
  {
    ytoB=Random(RH);
    xtoB=Random(RW);
    grabPixel=getPath.GetPixel(xtoB, ytoB);
  }
  
  
  getPath.Release();
  rainpath.Delete();
  
  
  
  //if (grabPixel!= -1 && grabPixel!=63488)
  //{
  //  Display("%d",grabPixel);
  //}
  
 // if (grabPixel!= -1)
 // {
    int d=currentrains;
    RainSplashParticles[d].y=ytoB;
    RainSplashParticles[d].x=xtoB;  
    RainSplashParticles[d].life=updatinvarl;
    RainSplashParticles[d].graphic=618+Random(2);
    RainSplashParticles[d].graphlay=0;
    RainSplashParticles[d].transparency=0;
    
    currentrains++;
    if (currentrains>=maxrains)
    {
      currentrains=0;
    }  
 // }
}

function RainSplashes(Object*rainer, int raindrops, int updateinterval, int rainpathgraphic, int howmany, int ID)
{
  int rainanimlay=3;
  rainpathgraph=rainpathgraphic;
  rainer.Clickable=false;
  maxrains=raindrops;
  updatinvarl=rainanimlay;
  
  
  int RH=Room.Height;
  int RW=Room.Width;
  
  if (raingraph[ID]!=null) raingraph[ID].Delete();
  raingraph[ID] = DynamicSprite.Create(RW, RH, true);
  DrawingSurface*getRain2;
  if (player.Room==1)
  {
    if (raingraph[0]!=null) raingraph[0].Delete();
    raingraph[0] = DynamicSprite.Create(280, 180, true);
    getRain2 = raingraph[0].GetDrawingSurface();
  }
  DrawingSurface*getRain = raingraph[ID].GetDrawingSurface();
  
  //CLEAR IT ALL
  getRain.Clear(COLOR_TRANSPARENT);
    
  if (updatedelay<=updateinterval)
  {
    updatedelay++;
  }
  else 
  {
    updatedelay=0;
    
    int k=0;
    while (k < howmany)
    {
      CreateRainSplashParticle(RW, RH);
      k++;
    }
  }
  
  
  int i=0;
  while (i < raindrops)
  {
    if (RainSplashParticles[i].life>0)
    {
      if (RainSplashParticles[i].graphic==623)
      {
        RainSplashParticles[i].life--;
      }      
      int pX=RainSplashParticles[i].x;
      int pY=RainSplashParticles[i].y;
      int pGraphic = RainSplashParticles[i].graphic;
      int pTrans = RainSplashParticles[i].transparency;
      if (pTrans>100) pTrans=100;
      
      if (player.Room==1 && pY-25<180 && pX-31<280)
      {
        getRain2.DrawImage(pX-31, pY-25, pGraphic, pTrans);
      }
      else 
      {
        getRain.DrawImage(pX-31, pY-25, pGraphic, pTrans);
      }
      RainSplashParticles[i].transparency+=2;
            
      if (RainSplashParticles[i].graphlay<rainanimlay)
      {
        RainSplashParticles[i].graphlay++;
      }
      else 
      {
        RainSplashParticles[i].graphlay=0;
        if (RainSplashParticles[i].graphic<623)
        {
          RainSplashParticles[i].graphic++;
        }
      }      
    }
    i++;
  }
  
  //getRain.Release();
  
  if (player.Room==1)
  {
    getRain2.DrawImage(0, 0, raingraph[0].Graphic);
    getRain2.Release();
    object[7].Graphic=raingraph[0].Graphic;
  }
  
  //getRain = Room.GetDrawingSurfaceForBackground();
  if (player.Room==9||player.Room==10||player.Room==12
  ||player.Room==13)
  {
  }
  else
  {
    getRain.DrawImage(0, 0, raingraph[ID].Graphic);
  }
  getRain.Release();
  rainer.Graphic=raingraph[ID].Graphic;
  
  //rainer.Graphic=raingraph.Graphic;
  
  
}


AudioChannel*MusicChannel[2];



function StartMusicChannels()
{
}




  
  
  
  






function on_event (EventType event, int data) 
{
  if (event == eEventEnterRoomBeforeFadein)
  {
    if (player.Room!=12 && player.Room!=1 && player.Room!=2
    && player.PreviousRoom==-1)
    {
        mouseEnable=true;
    }
    
    if (GetClip()==aDTLOOP)
    {
      Music_Stop(0);
    }
    
    waitforwalkable=false;
    gotdoubleclick=false;
    grabFearState=0;
    bfearmeter.Clickable=false;
    StartMusicChannels();
    if (Mouse.Visible)
    {
      UnvisibleMouse=20;
    }
    
  if (data==15 && cEgo.PreviousRoom!=16)
  {
    
    if (CanRideCicada)
    {
      SFX_Play(_Cicadaidle, -1);
      //CicadaLoop=aCicadaIdle.Play(eAudioPriorityVeryHigh, eRepeat);
      //CicadaLoop.SetRoomLocation(153, 151);
    }
    else 
    {
      SFX_Play(_UnbornCicada, -1);
     // CicadaLoop=aUnborncicada.Play(eAudioPriorityVeryHigh, eRepeat);
      //CicadaLoop.SetRoomLocation(193, 235);
    }
  }
  else if (data==16)
  {
    SFX_Play(_Cicadaidle, -1);
    SFX_SetPosition(_Cicadaidle, 0, 0, 0);
    //CicadaLoop=aCicadaIdle.Play(eAudioPriorityVeryHigh, eRepeat);
  }  
  else if (data==13)
  {
    
    //CicadaLoop=aAcidpuddle.Play(eAudioPriorityVeryHigh, eRepeat);
    //CicadaLoop.Volume=0;    
  }
  else 
  {
    SFX_Stop(_Cicadaidle, 3000);    //aCicadaIdle.Stop();
    SFX_Stop(_UnbornCicada, 3000);//aUnborncicada.Stop();
    //SFX_Stop(130, 3000);
    //aAcidpuddle.Stop();
    
  }
    
    
    if (!FreedDarkThing && !hasnotpressedstart && !Game.SkippingCutscene
    && murmurporttalk)
    {      
      dthing+=2000;      
    }
    
    
    
    
    
    
    
  }
  else if (event == eEventLeaveRoom)
  {
    RemoveSubtitle();
    currentraindrip=0;
    int i=2;
    while (i < 22)
    {
      character[i].Clickable=false;
      character[i].ChangeRoom(-1);
      i++;
    }
  }
  else if (event == eEventAddInventory)
  {
    if (data == iPaper1.ID || data == iPaper3.ID)
    {
      dScribe.SetOptionState(8, eOptionOffForever);
    }
  }
  
}


bool one=true, two=false;
bool hd=false;


bool FadeInX=false;
bool FadeOutX=false;


function MusicHandle()
{
  return;
  if (FadeOutX)
  {
    if (MVolume-1>((sldMusic.Value/2)+1))
    {
      MVolume-=1;
      Game.SetAudioTypeVolume(eAudioTypeMusic, MVolume, eVolExistingAndFuture);
    }
    else 
    {
      MVolume=(sldMusic.Value/2)+1;
      Game.SetAudioTypeVolume(eAudioTypeMusic, MVolume, eVolExistingAndFuture);
      FadeOutX=false;
      FadeInX=true;
      SetGameOption(OPT_CROSSFADEMUSIC, 1);
      MusicVid.Play(eAudioPriorityVeryHigh, eRepeat);
    }
  }
  
  if (FadeInX)
  {
    if (MVolume+1<sldMusic.Value)
    {
      if (sideShowMusicPlay && MVolume+1>85)
      {
        MVolume=85;
        Game.SetAudioTypeVolume(eAudioTypeMusic, MVolume, eVolExistingAndFuture);
        FadeOutX=false;
        FadeInX=false;
      }
      else if (WellBottomMusicPlay && MVolume+1>70)
      {
        MVolume=70;
        Game.SetAudioTypeVolume(eAudioTypeMusic, MVolume, eVolExistingAndFuture);
        FadeOutX=false;
        FadeInX=false;
      }
      else 
      {
        MVolume+=1;
        Game.SetAudioTypeVolume(eAudioTypeMusic, MVolume, eVolExistingAndFuture);
      }
      
    }
    else 
    {
      MVolume=sldMusic.Value;
      Game.SetAudioTypeVolume(eAudioTypeMusic, MVolume, eVolExistingAndFuture);
      FadeOutX=false;
      FadeInX=false;
    }
  }
  
}


int crossfadetimer=0;
AudioClip*toqueue;

function PlayClip(AudioClip*toplay)
{
  if (crossfadetimer>0)
  {
    toqueue=toplay;
    return;
  }
  MusicVid=toplay;
  
  int id=-1;
  
  //music0, music1, music2.music 22, music 23.music3,music5
  
  
  
  if (toplay==aFace1a) id=0;
  else if (toplay==aPurgatory1a) id=1;  
  else if (toplay==aWell) id=2;
  else if (toplay==aFurnace) id=3;
  else if (toplay==aMurmur1) id=4;
  else if (toplay==aCageLOOP) id=5;  
  else if (toplay==aYew1) id=6;
  else if (toplay==aYew2) id=7;
  else if (toplay==aCoaster1a) id=8;
  else if (toplay==aValkyrieLOOP) id=9;
  else if (toplay==aValkFurLOOP) id=10;
  else if (toplay==aKnotLOOP1b) id=11;
  else if (toplay==aWellBottomLOOP1b) id=12;
  else if (toplay==aSideshowInside1a) id=13;
  else if (toplay==aSideshowOutside1a) id=14;
  else if (toplay==aVisionsNU3b) id=15;
  else if (toplay==aVisionsNU2b) id=16;
  else if (toplay==aVisionsNU1b) id=17;
  else if (toplay==aDRHard1a) id=18;
  else if (toplay==aDTLOOP) id=19;
  else if (toplay==aDRSoft1a) id=20;
  else if (toplay==aDRSGLoop1a) id=21;
  else if (toplay==aSGLOOP) id=22;
  else if (toplay==aFJMusic) id=23;

  
  if (AfterRoomLoad)
  {
    AfterRoomLoad=false;
    crossfadetimer=3200/40;
    Music_Play(id, -1, 3000, 0, 0);
  }
  else 
  {
    crossfadetimer=3200/40;
    Music_Play(id, -1, 3000, 3000, 0);
  }
  
  /*
  
  if (Game.DoOnceOnly("introSongFEx"))
  {
    MusicVid.Play(eAudioPriorityVeryHigh, eRepeat);
    Game.SetAudioTypeVolume(eAudioTypeMusic, MVolume, eVolExistingAndFuture);
    MVolume=0;
    FadeInX=true;
  }
  else 
  {
    FadeOutX=true;
    MVolume=sldMusic.Value;
  }*/
  
  
}
  
int getFearState=-2;


bool StepSoundFix=false;
bool GotMouseOff=false;
int gTentBGDel=0;



function repeatedly_execute_always()
{
  cRenderScreech.x=320+GetViewportX();
  
  bmurmur.SetPosition(bportrait.X, bportrait.Y);
  
  
  if (crossfadetimer>0) crossfadetimer--;
  if (crossfadetimer==0)
  {
    if (toqueue!=null)
    {
      PlayClip(toqueue);
      toqueue=null;
    }
  }
  
  /*
    int h=0;
    bool foundacid=false;
    bool foundwindloop=false;
    String grabSounds="";
    while (h < System.AudioChannelCount)
    {
      AudioChannel*grabH = System.AudioChannels[h];
      if (grabH!=null && grabH.IsPlaying)
      {
        grabSounds=grabSounds.Append(String.Format("%d: is playing %d and Length is: %d[",h, grabH.IsPlaying, grabH.LengthMs));
        
        
        bool CicadaCond=(grabH.PlayingClip==aCicadaIdle || grabH.PlayingClip==aUnborncicada) && player.Room!=15 && player.Room!=16;
        bool AcidCond=grabH.PlayingClip==aAcidpuddle && player.Room!=13;
        if (grabH!=null && grabH.IsPlaying && grabH.PlayingClip==aAcidpuddle)
        {
          foundacid=true;
        }
        if (grabH!=null && grabH.IsPlaying && grabH.PlayingClip==aWindLoop)
        {
          foundwindloop=true;
          grabH.Volume=VolumeWind;
        }
        if (CicadaCond || AcidCond )
        {
          grabH.SetRoomLocation(0, 0);
          if (grabH.Volume-2>0) 
          {
            grabH.Volume-=2;
          }
          else 
          {
            grabH.Stop();
            grabH=null;
          }
        }
        else 
        {
          if (grabH!=null && grabH.IsPlaying && (grabH.PlayingClip==aCicadaIdle || grabH.PlayingClip==aUnborncicada
          || grabH.PlayingClip==aAcidpuddle))
          {
          if (DeadNpc)
          {
            if (grabH.Volume-2>0) 
            {
              grabH.Volume-=2;
            }
          }
          else 
          {
            if (grabH.Volume+2<sldSound.Value) 
            {
              if (grabH.PlayingClip==aAcidpuddle && gFade.Visible && gFade.Transparency<100)
              {
              }
              else  grabH.Volume+=2;
            }
            
          }
          }
          
          
        }
      }
      else grabSounds=grabSounds.Append(String.Format("%d: is playing %d and Length is: %d[",h, 0, 0));
      h++;
    }
    */
    if (player.Room==13 && object[10].Visible 
    && object[10].Transparency!=100)
    {
      int vel=FloatToInt((30.0*IntToFloat(sldSound.Value+1))/100.0, eRoundNearest);

      
      if (SFX_GetVolume(_Acidepuddle) + 2 < vel) SFX_SetVolume(_Acidepuddle, SFX_GetVolume(_Acidepuddle)+2);
      else SFX_SetVolume(_Acidepuddle, vel);

      SFX_SetPosition(_Acidepuddle, 967, 310, 120);
      //CicadaLoop=aAcidpuddle.Play(eAudioPriorityVeryHigh, eRepeat);
      //CicadaLoop.Volume=sldSound.Value;
    }
    else 
    {
      if (SFX_GetVolume(_Acidepuddle) - 2 > 0) SFX_SetVolume(_Acidepuddle, SFX_GetVolume(_Acidepuddle)-2);
      else SFX_SetVolume(_Acidepuddle, 0);
      //SFX_SetVolume(130, 0);
      //SFX_Stop(130, 1000);
    }
    
    int velx=FloatToInt((50.0*IntToFloat(sldSound.Value+1))/100.0, eRoundNearest);
    SFX_SetVolume(_Getitem1a, velx);
    
    
    //else SFX_SetPosition(130, 5000, 5000, 0);
    
    //*
    if (VolumeWind>0)
    {
      SFX_Play(_Windloop, -1);//
      SFX_SetVolume(_Windloop, VolumeWind);
      //aWindLoop.Play(eAudioPriorityHigh, eOnce);
    }
    if (VolumeWind==0)
    {
      SFX_Stop(_Windloop, 0);//aWindLoop.Stop();
    }
    //*/
    
  
  
  
  
  
  RemoveSubtitle();
  if (gTent.Visible)
  {
    gTentBGDel++;
    if (gTentBGDel >=4)//3
    {
    
    //2998-3018
    int g=btentbg.NormalGraphic;
    if (g < 3018)  g++;      
    else g=2998;      
    
    btentbg.NormalGraphic=g;
    gTentBGDel=0;
    }
  }
  
  
  if (!cDTSin1.Animating && cDTSin1.View!=DTSIGN1)
  {
    cDTSin1.LockViewFrame(DTIDLE, 0, 0);
    cDTSin1.Animate(0, 2, eOnce, eNoBlock, eForwards);
  }
  
  
  if (!FreedDarkThing && player.Room==7)
  {
      //cDTRip.ManualScaling=true;
      //cDTRip.Scaling=100;
      //cDTRip.ChangeRoom(7);
      cDTRip.Clickable=false;    
      cDTRip.x=cDarkThing.x;
      cDTRip.y=cDarkThing.y;
      
  if (cDarkThing.Animating && cDTRip.Animating==false)
  {
    if (cDarkThing.View==DARKTHINGV)
    {
      cDTRip.LockViewFrame(DTRIPPLE, 0, 0);
      cDTRip.Animate(0, 3, eOnce, eNoBlock, eForwards);
    }
    if (cDarkThing.View==DTSCRCAGE)
    {
      cDTRip.LockViewFrame(DTRIPPLE,1, 0);
      cDTRip.Animate(1, 3, eOnce, eNoBlock, eForwards);
    }
  }
  
  }
  
  
  if (UnvisibleMouse>0)
  {
    if (Mouse.Visible)
    {
      Mouse.Visible=false;
      GotMouseOff=true;
    }
    UnvisibleMouse--;
  }
  else 
  {
    if (GotMouseOff)
    {
      GotMouseOff=false;
      Mouse.Visible=!ScrollingMouse&& !ScreechOccuring && mouseEnable;//true;
    }
    else 
    {
      if (gFade.Visible && gFade.Transparency<100)
      {
        Mouse.Visible=false;
      }
      else
      {
        if (player.Room!=12)
        {
          Mouse.Visible=!ScrollingMouse&& !ScreechOccuring&& mouseEnable;//true;
        }
      }      
    }
  }
  
  
  
  
  
  if (grabFearState!=-1 && getFearState!=grabFearState)
  {
    bfearmeter.Animate(FEARMVIEW,grabFearState, 1, eRepeat);
    getFearState=grabFearState;
  }
  
  //cEgo.SayBackground(String.Format("%d",cEgo.View));
  if (cEgo.View==VIEW28 && cEgo.Transparency<50)
  {
    int da, db;
    if (cEgo.Loop==0 || cEgo.Loop==3){ da=4; db=10;}
    else {da=5; db=11;}
    
    if (cEgo.Frame==da || cEgo.Frame==db)
    {
      if (!StepSoundFix)
      {
        int bsound=Random(100);
        if (bsound <= 25) SFX_Play(_Stepone1a, 0);
        else if (bsound <= 50)SFX_Play(_StepThree1a, 0);
        else if (bsound <= 75)SFX_Play(_StepTwo1a, 0);
        else SFX_Play(_StepFour1a, 0);
        StepSoundFix=true;
      }
    }
    else 
    {
      StepSoundFix=false;
    }
  }
  else
  {
    StepSoundFix=false;
  }
  
  if (cWoman.View==WOMANWALK && player.Room==3)
  {
    int da, db;
    da=4;
    db=13;
    
    if (cWoman.Frame==da || cWoman.Frame==db)
    {
      int bsound=Random(100);
      if (bsound <= 35) SFX_Play(_Wstepone1a, 0);
      else if (bsound <= 70)SFX_Play(_Wstepthree1a, 0);
      else SFX_Play(_WstepTwo1a, 0);
    }
  }
  
  
 
 
 String grabChans;
 grabChans="";
 int d=0;
 int grabso[9];
  int Lgrabso[9];
  
  
 
          
     
     /*
     else if (grabChan.PlayingClip == aSideshowInside1a || grabChan.PlayingClip == aSideshowOutside1a)
     {
       sideShowMusicPlay=true;
        grabChan.Volume=FloatToInt((85.0*IntToFloat(sldMusic.Value+1))/100.0, eRoundNearest);
     }
     else if (grabChan.PlayingClip == aWellBottomLOOP1b)
     {
       WellBottomMusicPlay=true;
       grabChan.Volume=FloatToInt((70.0*IntToFloat(sldMusic.Value+1))/100.0, eRoundNearest);
     }
     
     */
   
  
  
  
  int k=1;
      
    
  while (k < 18)
  {
    int vel=FloatToInt((50.0*IntToFloat(sldSound.Value+1))/100.0, eRoundNearest);
    SFX_SetVolume(k, vel);
     
    k++;
  }
  k=22;
  while (k < 27)
  {
    int vel=FloatToInt((50.0*IntToFloat(sldSound.Value+1))/100.0, eRoundNearest);
    SFX_SetVolume(k, vel);
    k++;
  }
  int vel=FloatToInt((80.0*IntToFloat(sldSound.Value+1))/100.0, eRoundNearest);
  SFX_SetVolume(_Basicclick1a, vel);
  
  vel=FloatToInt((75.0*IntToFloat(sldSound.Value+1))/100.0, eRoundNearest);
  SFX_SetVolume(_Clickback1a, vel);
  
  vel=FloatToInt((55.0*IntToFloat(sldSound.Value+1))/100.0, eRoundNearest);
  SFX_SetVolume(_Basicclick1a, vel);
  
  vel=FloatToInt((55.0*IntToFloat(sldSound.Value+1))/100.0, eRoundNearest);
  SFX_SetVolume(_NUdrawerlocked, vel);
  
  
  
  if (player.Room==5 || player.Room==8|| player.Room==14)
  {
    vel=FloatToInt((10.0*IntToFloat(sldSound.Value+1))/100.0, eRoundNearest);
    SFX_SetVolume(_Justring, vel);
  }
  
  k=60;
  while (k < 69)
  {
    vel=FloatToInt((30.0*IntToFloat(sldSound.Value+1))/100.0, eRoundNearest);
    SFX_SetVolume(k, vel);
    k++;
  }
  
  vel=FloatToInt((50.0*IntToFloat(sldSound.Value+1))/100.0, eRoundNearest);
  SFX_SetVolume(_ATongueroll1a, vel);
  
  
  k=103;
  while (k<111)
  {
    vel=FloatToInt((60.0*IntToFloat(sldSound.Value+1))/100.0, eRoundNearest);
    SFX_SetVolume(k, vel);
    k++;
  }
  
  
  
  k=217;
  while (k<221)
  {
    float GetScal = IntToFloat(cEgo.Scaling);
    float byscal = (GetScal/100.0);
    byscal = byscal*50.0;
    int setVol = FloatToInt(byscal, eRoundNearest);
    if (setVol <0) setVol=0;
    if (setVol > 100) setVol=100;

    vel=FloatToInt((IntToFloat(setVol)*IntToFloat(sldSound.Value+1))/100.0, eRoundNearest);
    SFX_SetVolume(k, vel);
    k++;
  }
  
  
  int FinalVolumeSFX=FloatToInt((IntToFloat(sldMasterVolume.Value+1)*IntToFloat(sldSound.Value+1))/100.0, eRoundNearest);
  
  //sldSound.Value
  SFX_SetGlobalVolume(FinalVolumeSFX);
  
  
  
  int FinalVolumeMFX=FloatToInt((IntToFloat(sldMasterVolume.Value+1)*IntToFloat(sldMusic.Value+1))/100.0, eRoundNearest);
  
  
  int MusicSliderValue=FinalVolumeMFX;//sldMusic.Value;
  if (MusicVid==aMurmur1)
  {
    if (MurmurProphecy)
    {
      if (Music_GetVolume()-2>0)Music_SetVolume(Music_GetVolume()-2);
      else Music_SetVolume(0);
    }
    else 
    {
      if (Music_GetVolume()+2<MusicSliderValue)Music_SetVolume(Music_GetVolume()+2);
      else Music_SetVolume(MusicSliderValue);
    }
  }
  else if (MusicVid == aSideshowInside1a || MusicVid == aSideshowOutside1a)
  {
    int mVol = FloatToInt((85.0*IntToFloat(MusicSliderValue+1))/100.0, eRoundNearest);
    Music_SetVolume(mVol);
  }
  else if (MusicVid == aWellBottomLOOP1b)
  {
    int mVol=FloatToInt((70.0*IntToFloat(MusicSliderValue+1))/100.0, eRoundNearest);
    Music_SetVolume(mVol);
  }
  else 
  {
    Music_SetVolume(MusicSliderValue);
  }
  
  
  
  
    
  
  
  
  
 /*
 
 while (d < System.AudioChannelCount)
 {
   AudioChannel*grabChan = System.AudioChannels[d];
  
   
   if (grabChan.IsPlaying)
   {
     if (grabChan.PlayingClip == aSGglow1a
     || grabChan.PlayingClip == aSGhit1a
     || grabChan.PlayingClip == aSGlose1a
     || grabChan.PlayingClip == aSGshot1a
     || grabChan.PlayingClip == aSGSound1a
     || grabChan.PlayingClip == aSGwin1a
     || grabChan.PlayingClip == aDraweropen1a        
     
     || grabChan.PlayingClip ==aCoasterarriving1a
     || grabChan.PlayingClip ==aCoasterleavingcage1a
     || grabChan.PlayingClip == aCoasterleavingphone1a
     || grabChan.PlayingClip == aCoastertravel1a1
     
     || grabChan.PlayingClip == aCoastertravel1b
     
     || grabChan.PlayingClip == aBoothHangup1a
     || grabChan.PlayingClip == aBoothPickup1a
     || grabChan.PlayingClip == aDialtone1a
     || grabChan.PlayingClip == aHandsetHangup1a
     || grabChan.PlayingClip == aHandsetPickup1a
     || grabChan.PlayingClip == aRing1a
     || grabChan.PlayingClip == aTalkloop1a
     || grabChan.PlayingClip == aToneOne1a
     || grabChan.PlayingClip == aToneThree1a
     || grabChan.PlayingClip == aToneTwo1a)
     {
        grabChan.Volume=FloatToInt((50.0*IntToFloat(sldSound.Value+1))/100.0, eRoundNearest);
     }
     else if (grabChan.PlayingClip == aBasicclick1a)
     {
        grabChan.Volume=FloatToInt((80.0*IntToFloat(sldSound.Value+1))/100.0, eRoundNearest);
     }
     else if (grabChan.PlayingClip == aSideshowInside1a || grabChan.PlayingClip == aSideshowOutside1a)
     {
       sideShowMusicPlay=true;
        grabChan.Volume=FloatToInt((85.0*IntToFloat(sldMusic.Value+1))/100.0, eRoundNearest);
     }
     else if (grabChan.PlayingClip == aWellBottomLOOP1b)
     {
       WellBottomMusicPlay=true;
       grabChan.Volume=FloatToInt((70.0*IntToFloat(sldMusic.Value+1))/100.0, eRoundNearest);
     }
     
     
     else if (grabChan.PlayingClip == aMurmurlips1
     || grabChan.PlayingClip == aMurmurlips2
     || grabChan.PlayingClip == aMurmurlips3
     || grabChan.PlayingClip == aMurmurlips4
     || grabChan.PlayingClip == aMurmurlips5
     || grabChan.PlayingClip == aMurmurlips6
     || grabChan.PlayingClip == aMurmurlips7
     || grabChan.PlayingClip == aMurmurlips8)
     {
        grabChan.Volume=FloatToInt((60.0*IntToFloat(sldSound.Value+1))/100.0, eRoundNearest);
     }
     
     else if (grabChan.PlayingClip ==aClickback1a)
     {
        grabChan.Volume=FloatToInt((75.0*IntToFloat(sldSound.Value+1))/100.0, eRoundNearest);
     }     
     else if (grabChan.PlayingClip==aRing1a 
     && (player.Room==5 || player.Room==8|| player.Room==14))
     {
       grabChan.Volume=FloatToInt((10.0*IntToFloat(sldSound.Value+1))/100.0, eRoundNearest);
     }
          
     else if (grabChan.PlayingClip == aStepFour1a
     || grabChan.PlayingClip == aStepOne1a
     || grabChan.PlayingClip == aStepTwo1a
     || grabChan.PlayingClip == aStepOne1a)
     {
       float GetScal = IntToFloat(cEgo.Scaling);
       float byscal = (GetScal/100.0);

       byscal = byscal*50.0;
       int setVol = FloatToInt(byscal, eRoundNearest);
       if (setVol <0) setVol=0;
       if (setVol > 100) setVol=100;

       grabChan.Volume=FloatToInt((IntToFloat(setVol)*IntToFloat(sldSound.Value+1))/100.0, eRoundNearest);

       
     }
     if (grabChan.PlayingClip.Type== eAudioTypeAmbientSound ||
     grabChan.PlayingClip.Type== eAudioTypeSound)
     {
       if (grabChan.Volume>sldSound.Value)
       {
         grabChan.Volume=sldSound.Value;
       }
     }
     if (grabChan.PlayingClip.Type== eAudioTypeMusic)
     {
       if (grabChan.PlayingClip ==aMurmur1)
       {
         if (MurmurProphecy)
         {
           if (grabChan.Volume-2>0)grabChan.Volume-=2;
           else grabChan.Volume=0;
         }
         else 
         {
           if (grabChan.Volume+2<sldMusic.Value)grabChan.Volume+=2;
           else grabChan.Volume=sldMusic.Value;
         }
       }
       else 
       {
         if (grabChan.Volume>sldMusic.Value)
         {
           grabChan.Volume=sldMusic.Value;
         }
       }
     }
   } 
   d++;
 }

  MusicHandle();
  */
  
  
  
}


