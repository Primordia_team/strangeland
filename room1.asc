
DynamicSprite*pebbles;
bool SaveGamesExist=false;
function room_Load()
{
  if (CheckIfSaveGamesExist())    
  {
    SaveGamesExist=true;
  }
  
  //if (Game.DoOnceOnly("PebbleGraphAdjustment"))
  //{
  //  PebbleGraph=DynamicSprite.CreateFromExistingSprite(2004, true);
  //  PebbleGraph.Crop(0, 0, PebbleGraph.Width-30, PebbleGraph.Height);
  //}
  
  //opebblesover.Graphic=PebbleGraph.Graphic;
  
  
  RemoveWalkableArea(2);
  omauve.SetView(MAUVE, 0, Random(28));
  omauve.Animate(0, 3, eRepeat, eNoBlock, eForwards);
  
  //ADDED CROWS TO PURG HERE
  oPcrows.SetView(45);
  oPcrows.Animate(0, 3, eRepeat, eNoBlock, eForwards);
  

  
  disableblackdogevents=false;
  
  //if (pebbles==null)
  //{
    pebbles = DynamicSprite.Create(70, 40, true);
    DrawingSurface*rr=pebbles.GetDrawingSurface();
    rr.Clear(16);
    rr.Release();
    oPebbles.Graphic=pebbles.Graphic;
    oPebbles.Transparency=99;//100;
  //}
  
  
  
  obg1.SetView(PURGBGANIMS, 0, 0);
  obg1.Animate(0, 3, eRepeat, eNoBlock, eForwards);
  
  obg4.SetView(PURGBGANIMS, 1, 0);
  obg4.Animate(1, 3, eRepeat, eNoBlock, eForwards);
  
  obg5.SetView(PURGBGANIMS, 3, 0);
  obg5.Animate(3, 3, eRepeat, eNoBlock, eForwards);
  
  obg5.Baseline=4;
  
  if (Game.DoOnceOnly("StartPosEgo"))
  {
    hHotspot1.Enabled=false;
     cEgo.Transparency=100;
     cEgo.x=768;
     cEgo.y=235;  
     obg5.SetView(PURGBGANIMS, 3, 0);
     obg5.Animate(3, 3, eRepeat, eNoBlock, eForwards);
     gFade.Transparency=0;
     gFade.Visible=true;
     cEgo.FaceDirection(eDirectionDownLeft, eBlock);
  }
  else 
  {
    hHotspot1.Enabled=true;
    obg5.SetView(PURGBGANIMS, 4, 0);
    obg5.Animate(4, 3, eRepeat, eNoBlock, eForwards);
    oexit.Visible=false;
    ostart.Visible=false;
    oload.Visible=false;
    ooptions.Visible=false;
    oloadNonClick.Visible=false;
    ooptionshover.Visible=false;
  }
  
}

bool LightningFlash=false;
function repeatedly_execute_always()
{
  //hHotspot1.Enabled=!ostart.Visible;
  if (hasnotpressedstart)
  {
    if (!SaveGamesExist)
    {
      oload.Visible=false;
      oloadNonClick.Visible=false;
    }
  }
  
  if (omauve.View==MAUVE && !PlayCutsceneRoom1)
  {    
    
    if (omauve.Frame==1 || omauve.Frame==11 || omauve.Frame==20) 
    {
      if (!LightningFlash)
      {    
        LightningSound(Random(100));        
        
        LightningFlash=true;
      }
    }
    else 
    {
      LightningFlash=false;
    }
  }
  else
  {
    LightningFlash=false;
  }
  
  
  
  
  if (GetWalkableAreaAt(cEgo.x, cEgo.y)==1)
  {
    cEgo.ManualScaling=false;
  }
  
  
  oPebbles.Clickable=!hasnotpressedstart;
  
  if (!waitmode && hasnotpressedstart)
  {
    
    cEgo.FaceRotate(mouse.x+GetViewportX(), mouse.y-GetViewportY());
  }
  
  
  ParallaxUpdate();
  ostart.X=(obg5.X+193)-1;//216
  ostart.Y=obg5.Y-222;
  
  oexit.X=obg5.X+439-23;
  oexit.Y=(obg5.Y-175)-1;
  
  oload.X=obg5.X+392-23;
  oload.Y=(obg5.Y-95);
  
  ooptions.X=obg5.X+310;
  ooptions.Y=(obg5.Y);
  
  oloadNonClick.X=oload.X;
  oloadNonClick.Y=oload.Y;
  
  ooptionshover.X=ooptions.X;
  ooptionshover.Y=ooptions.Y;
  
  
  oPebbles.X=obg5.X+270-23;
  oPebbles.Y=obg5.Y-222+133;
  
  opebblesover.X=oPebbles.X;
  opebblesover.Y=oPebbles.Y;
  opebblesover.Baseline=oPebbles.Baseline+1;
  
  ostart.Baseline=5;
  oexit.Baseline=5;
  oload.Baseline=6;
  ooptions.Baseline=7;
   oloadNonClick.Baseline=5;
   ooptionshover.Baseline=8;
  
  
  if (!waitmode)
  {
  int getxx=mouse.x;//+GetViewportX();
  int getyy=mouse.y;//+GetViewportY();
  Object*grabO = Object.GetAtScreenXY(getxx, getyy);
  if (grabO==ostart)
  {
    oexit.Transparency=100;
    ostart.Transparency=0;
    oload.Transparency=100;
    ooptionshover.Transparency=100;
  }
  else if (grabO==oexit)
  {
    oexit.Transparency=0;
    ostart.Transparency=100;
    oload.Transparency=100;
    ooptionshover.Transparency=100;
  }
  else if (grabO==oload)
  {
    oexit.Transparency=100;
    ostart.Transparency=100;
    oload.Transparency=0;
    ooptionshover.Transparency=100;
  }
  else if (grabO==ooptions)
  {
    ostart.Transparency=100;
    oexit.Transparency=100;
    oload.Transparency=100;
    ooptionshover.Transparency=0;
  }
  else 
  {
    ostart.Transparency=100;
    oexit.Transparency=100;
    oload.Transparency=100;
    ooptionshover.Transparency=100;
  }
  }
  
  if (!PlayCutsceneRoom1)
  {
  
  cEgo.ManualScaling=true;
  int grabscal = 30;
  //0-587
  if (cEgo.x > 670)//550)
  {
    grabscal = 87;//61
    grabscal=grabscal+( (cEgo.x-75)/50);
  }
  //else if (cEgo.x>390 && cEgo.x<=550)
  //{
  //  grabscal=110;
  //  grabscal=grabscal+( (cEgo.x-700)/4);
  //}
  else if (cEgo.x>380  && cEgo.x<=670)//550 )
  {
    //grabscal=16;
    grabscal=110;
    grabscal=grabscal+( (cEgo.x-700)/4);
    if (grabscal<5) grabscal=5;
  }  
  else if (cEgo.x<=380)
  {
    //grabscal=16;
    grabscal=110;
    grabscal=grabscal+( (cEgo.x-720)/4);
    if (grabscal<12) grabscal=12;
  } 
  cEgo.Scaling=grabscal;
 // cEgo.SayBackground(String.Format("%d",grabscal));
  }
  //ostart.Graphic
  
}





function noloopcheck room_AfterFadeIn()
{
  
  if (gDialogs.Visible)
  {
    gDialogs.Visible=false;
  }
  
  if (PlayCutsceneRoom1)
  {
    //
      StartCutscene(eSkipESCOnly);
      
      int base = cEgo.Baseline;
      cEgo.Baseline=6000;
      bool grab = cEgo.ManualScaling;
      cEgo.ManualScaling=true;
      cEgo.Scaling=7;
      
      //int dy=FloatToInt((244.0*IntToFloat(cEgo.Scaling))/100.0, eRoundNearest);
      //int dx=FloatToInt((20.0*IntToFloat(cEgo.Scaling))/100.0, eRoundNearest);
      //cEgo.x=286+dx;
      //cEgo.y=52+dy;
      
      cEgo.x=287;
      cEgo.y=57;
      cEgo.FaceDirection(eDirectionDownLeft, eBlock);      
      WaitC(40);
      
      cEgo.Animate(6, 3, eOnce, eNoBlock, eBackwards);
      cEgo.TweenPosition(0.5, 296, 57, eLinearTween, eBlockTween);
      cEgo.FaceDirection(eDirectionUpRight, eBlock);
      WaitC(20);
      
      
      int px=cEgo.x;
      int py=cEgo.y;
      
      int fx=317;
      int fy=133;
      
      
      //variables
      float xvel,yvel,xdiff,ydiff,vydiff;
      float accel=0.07;//0.12;
      
      //velocity in pixels per second.
      xvel=0.8; //doesn't change (ignoring friction).
      yvel=0.1;//0.8; //negative for upwards.
      xdiff=0.0; //this stores fractional changes in x coordinate.
      ydiff=0.0; // '' in y coordinate.
      vydiff=0.0; // '' in y component of velocity.
      
      gBlack.Transparency=100;
      gBlack.Visible=true;
      
      cEgo.TweenScaling(0.8, 5, eLinearTween, eNoBlockTween);
      int i=0;
      int pgx=player.x;
      float enx=0.0;
      while (i <200)
      {
        if (Pause)
        {
           
        }
        else 
        {
        float a,b;
        vydiff+=accel;
        a=vydiff;
        if(a!=0.0)
        {
          yvel+=a;
          vydiff-=a;
        }
        xdiff+=xvel;
        a=xdiff;
        ydiff+=yvel;
        b=ydiff;
        if((a+b)!=0.0)
        {
          enx+=a;
          int gA=FloatToInt(a);
          int gB=FloatToInt(b);
          player.x=pgx+FloatToInt(enx);
          
          if (player.y + gB > fy) player.y=fy;
          else player.y+=gB;
          xdiff-=a;
          ydiff-=b;
        }
        if (player.y==fy)
        {
          i=201;
        }
        
        if (i ==22)
        {
          gBlack.TweenTransparency(0.5, 0, eLinearTween, eNoBlockTween);
        }
        
        i++;
        }
        Wait(1);
      }
      
      SFX_Play(_Breakbranch1a, 0);//aBreakbranch1a.Play();
      WaitC(140);

      
      

      //cEgo.LockViewFrame(FALLINGCAGE, 0, 0);
      //WaitC(40);
      //cEgo.Animate(0, 3, eOnce, eNoBlock, eForwards);
      //cEgo.TweenPosition(0.5, cEgo.x+15, cEgo.y, eLinearTween, eNoBlockTween);      
      //WaitC(1000);
      /*      
      SHRIEKING IS STILL HAPPENING
      
      Cut to zoomed out view, specifically, purgatory background, the left part.  
      STILL SHRIEKING
      cEgo falls off and hits yew on the way down (sound of breaking branch as it fades to black).  
      STILL SHRIEKING, 
      until the branch breaks, 
      then silence
      
      cEgo respawns
      */
      GenericDeath();
      cEgo.UnlockView();
      cEgo.ManualScaling=grab;
      cEgo.Baseline=base;
      cEgo.ChangeRoom(1, 768, 235);
      PlayCutsceneRoom1=false;
      EndCutscene();
      return;
  }
  
  
  if (Game.DoOnceOnly("IntroStrangelandP1"))
  {
    // StartCutscene(eSkipAnyKey);
    cEgo.LockViewFrame(EGOSTAND,0, 0);
    cEgo.Transparency=0;
    cEgo.ManualScaling=true;
    cEgo.Scaling=90;
    RemoveWalkableArea(1);
    oPcrows.TweenPosition(5.5, 402, 160, eLinearTween, eNoBlockTween);
    
    gFade.TweenTransparency(1.5, 100, eEaseOutTween, eBlockTween);
    gFade.Visible=false;
   // EndCutscene();
    
  }
  
  if (diedfromdog)
  {    
   
   PlayClip(aPurgatory1a);
  }
  else 
  {
    if (player.PreviousRoom!=12)
    {
      //WaitC(4);
      PlayClip(aPurgatory1a);
    }
  }
  if (Game.DoOnceOnly("IntroStrangelandPXXX"))
  {
    //StartCutscene(eSkipAnyKey);
    
    omauve.Baseline=2;
    cEgo.Animate(cEgo.Loop, 2, eOnce, eNoBlock, eForwards);
    int a=0;
    while (cEgo.Animating)
    {
      if (a==150)
      {
        oPcrows.TweenTransparency(5.5, 100, eLinearTween, eNoBlockTween);
        oPcrows.TweenPosition(5.5, 257, 115, eLinearTween, eNoBlockTween);
      }
      //cEgo.SayBackground(String.Format("%d",a));
      if (cEgo.Frame==20)
      {
        SFX_Play(_Unbounda, 0);//aUnbounda.Play();
      }
      a++;
      WaitC(1);
    }
    cEgo.UnlockView();
    cEgo.FaceDirection(eDirectionDownLeft);
    
    int Lorp = cEgo.Loop;
    cEgo.LockViewFrame(EGOHAIR,0, 0);
    cEgo.Animate(cEgo.Loop, 2, eOnce, eBlock, eForwards);
    cEgo.UnlockView();
    cEgo.Loop=Lorp;   
    mouse.Visible=true;
    EndCutscene();
    mouseEnable=true;
  }
  if (hasdiedwell|| diedfromwell || diedcageroom || diedfromstab || diedfromdog || diedfromfire)
  {
   // if (!Game.InSkippableCutscene)
  //  {
    
   // }
    StartCutscene(eSkipESCOnly);
    SFX_Play(_Reincarnate, 0);//aReincarnate.Play();
    cEgo.LockViewFrame(EGOMATER, 0, 0);    
    gFade.TweenTransparency(1.5, 100, eEaseOutTween, eBlockTween);
    gFade.Visible=false;
    
    
    
    cEgo.Animate(cEgo.Loop, 3, eOnce, eBlock, eForwards);
    
    
    cEgo.UnlockView();
    cEgo.FaceDirection(eDirectionDownLeft, eBlock);
    if (Game.DoOnceOnly("FirstEntryRoom1Purg"))
    {
    WaitC(40);
    cEgo.FaceDirection(eDirectionRight, eBlock);
    WaitC(40);
    cEgo.FaceDirection(eDirectionLeft, eBlock);
    WaitC(40);
    cEgo.Speak("What ... happened ... ?");    
    }
    else 
    {
      if (knifedeath)
      {
        if (Game.DoOnceOnly("FirstEntryRoom1PurgKNIFE"))
        {
          cEgo.Speak("Well, it was worth a try.");
        }
        else 
        {
          cEgo.Speak("That's not gonna cut it.");
        }
        knifedeath=false;
      }
      else if (openedthecageanddied)
      {
        openedthecageanddied=false;
        cEgo.Speak("Ugh...");
        
        
        cEgo.LockViewFrame(EGOGRIM, 1, 0);//12
        cEgo.Animate(cEgo.Loop, 3, eOnce, eBlock, eForwards);
        cEgo.UnlockView();
        cEgo.FaceDirection(eDirectionDownLeft, eBlock);
        
        cEgo.Speak("I think I hit that tree on the way down.");
      }
      else if (justdiedfromdarkthing)
      {        
        cEgo.Speak("Aagghhh!");
      }
      if (cEgo.PreviousRoom==3 && !justdiedfromdarkthing)
      {
        if (Game.DoOnceOnly("FirstEntryRoom1PurgWELL"))
        {
          cEgo.Speak("I never even hit bottom...");
        }
        else 
        {
          cEgo.Speak("Not again.");
        }
      }
      else if (cEgo.PreviousRoom==6 && !justdiedfromdarkthing)
      {
        if (Game.DoOnceOnly("FirstEntryRoom1PurgDOG"))
        {
          cEgo.Speak("Talk about the dog that bit you.");
        }
        else 
        {
          cEgo.Speak("I can still feel its teeth.");
        }
      }
      if (DiedSeastarSpit)
      {
        DiedSeastarSpit=false;
        cEgo.Speak("That was unpleasant.");
      }
      justdiedfromdarkthing=false;
    }
    
   if (diejumpedoff)
   {
     cEgo.FaceDirection(eDirectionLeft, eBlock);
     gFade.TweenTransparency(1.5, 100, eEaseOutTween, eBlockTween);
     gFade.Visible=false;
   }
   EndCutscene();
  }
  else 
  {
    if (Game.DoOnceOnly("IntroStrangelandPXXXTGRTY"))
    {
     // SetGameOption(OPT_TURNWHENFACING , 1);
      //cEgo.FaceDirection(eDirectionDown, eBlock);
      //cEgo.FaceRotate(mouse.x+GetViewportX(), mouse.y-GetViewportY());
      //SetGameOption(OPT_TURNWHENFACING , 0);
      
        cEgo.StopMoving();
        int xdim=40;
        int getLoop=0;
        
        
        int wDelay=5;
        
        if (mouse.y-GetViewportY()<cEgo.y-80)
        {
          //UP
          if (mouse.x+GetViewportX()<cEgo.x-xdim) getLoop=7;
          else if (mouse.x+GetViewportX()>cEgo.x+xdim) getLoop=5;
          else getLoop=3;
        }
        else if (mouse.y-GetViewportY()>cEgo.y+80)
        {
          //DOWN
          if (mouse.x+GetViewportX()<cEgo.x-xdim) getLoop=6;
          else if (mouse.x+GetViewportX()>cEgo.x+xdim) getLoop=4;
          else getLoop=0;
        }
        else 
        {
          if (mouse.x+GetViewportX()<cEgo.x-xdim) getLoop=1;
          if (mouse.x+GetViewportX()>cEgo.x+xdim) getLoop=2;
        }
        
       if (getLoop==6 || getLoop==0 || getLoop==1)
       {
         cEgo.FaceRotate(mouse.x+GetViewportX(), mouse.y-GetViewportY());
       }
       else if (getLoop==7)
       {
         cEgo.FaceDirection(eDirectionLeft, eBlock);
         WaitC(wDelay);
         cEgo.FaceDirection(eDirectionUpLeft, eBlock);
         WaitC(wDelay);
       }
       else if (getLoop==4)
       {
         cEgo.FaceDirection(eDirectionDown, eBlock);
         WaitC(wDelay);
         cEgo.FaceDirection(eDirectionDownRight, eBlock);
         WaitC(wDelay);
       }
       else if (getLoop==5 || getLoop==3)
       {
         cEgo.FaceDirection(eDirectionLeft, eBlock);
         WaitC(wDelay);
         cEgo.FaceDirection(eDirectionUpLeft, eBlock);
         WaitC(wDelay);
         cEgo.FaceDirection(eDirectionUp, eBlock);
         WaitC(wDelay);
         if (getLoop==5)
         {
           cEgo.FaceDirection(eDirectionUpRight, eBlock);
           WaitC(wDelay);
         }
       }
       else if (getLoop==2)
       {
         cEgo.FaceDirection(eDirectionDown, eBlock);
         WaitC(wDelay);
         cEgo.FaceDirection(eDirectionDownRight, eBlock);
         WaitC(wDelay);
         cEgo.FaceDirection(eDirectionRight, eBlock);
         WaitC(wDelay);      
       }
       
       
      
      
      
    }
    else
    {
      gFade.TweenTransparency(1.5, 100, eEaseOutTween, eBlockTween);
    }
  }
  diedfromdog=false;
  
  omauve.Baseline=2;
}

function oPebbles_Interact()
{
  if (player.HasInventory(iPebbles))
  {
    cEgo.FaceObject(oPebbles, eBlock);
    cEgo.Speak("I've got plenty.");
  }
  else 
  {
    cEgo.Walk(oPebbles.X+20, oPebbles.Y+80, eBlock, eWalkableAreas);
    
    //cEgo.FaceDirection(eDirectionDown, eBlock);
    cEgo.FaceDirection(eDirectionUp, eBlock);
    cEgo.PickUp();
    SFX_Play(_GetPebbles1a, 0);//aGetpebbles1a.Play();
    opebblesover.Visible=false;
    SFX_Play(_Getitem1a, 0);//agetitem1a
    cEgo.AddInventory(iPebbles);
    cEgo.PickUp();
    
    
  }
}

function oPebbles_Look()
{
  cEgo.FaceObject(oPebbles, eBlock);
  cEgo.Speak("Just some pebbles.");
}
function room_Leave()
{
 // pebbles.Delete();
  cEgo.ManualScaling=false;
}

function hHotspot1_Interact()
{
  hasnotpressedstart=false;

  if (Game.DoOnceOnly("IntroWaitdx"))
  {
  StartCutscene(eSkipESCOnly);
  }
  
  if (!gotdoubleclick)
  {    
  
  int getCego = cEgo.WalkSpeedX;
  cEgo.StopMoving();  
  cEgo.SetWalkSpeed(2, 2);
  cEgo.ScaleMoveSpeed=true;//AdjustScalingWithSpeed
  
  cEgo.FaceDirection(eDirectionLeft, eBlock);
  
  
  if (Game.DoOnceOnly("IntroWaitdx34a42"))
  {
    TweenViewportX(4.8, 0, eEaseInTween, eNoBlockTween);

    player.Walk(650, 191, eBlock, eWalkableAreas);
    player.Walk(507, 191, eNoBlock, eWalkableAreas); 
    WaitC((3*4)+22);
  }
  else 
  {
    
  
    
    
    if (!gotdoubleclick)
    {
      player.Walk(507, 191, eNoBlock, eWalkableAreas); 
      WaitC(3*4);    
      TweenViewportX(2.5, 0, eEaseInTween, eNoBlockTween);
      gFade.TweenTransparency(1.8, 0, eLinearTween, eBlockTween);
      cEgo.StopMoving();  
      cEgo.SetWalkSpeed(getCego, getCego);
      gFade.Visible=true;
      gFade.Transparency=100;
      cEgo.ScaleMoveSpeed=false;//AdjustScalingWithSpeed
      cEgo.ChangeRoom(2, 409, 420);//389, 351);*/
    }
    else 
    {
     
    }
    return;
    //player.Walk(650, 191, eBlock, eWalkableAreas);
  }
  
  gFade.TweenTransparency(1.8, 0, eLinearTween, eBlockTween);
  cEgo.StopMoving();  
  cEgo.SetWalkSpeed(getCego, getCego);
  if (Game.DoOnceOnly("IntroWaitdx34a42"))
  {
  }
  else 
  {
    WaitC(30);
  }
  
  gFade.Visible=true;
  gFade.Transparency=100;
  
  cEgo.ScaleMoveSpeed=false;//AdjustScalingWithSpeed
    
  //cEgo.FaceDirection(eDirectionUp, eBlock);
  if (Game.DoOnceOnly("IntroWait"))
  {
   // WaitC(80);
  }
  }
  cEgo.ChangeRoom(2, 409, 420);//389, 351);
}

function ostart_Interact()
{
  mouseEnable=false;
  RestoreWalkableArea(1);
  SFX_Play(_Clickokay, 0);//aClickokay.Play
  hHotspot1.RunInteraction(eModeInteract);
}

function oexit_Interact()
{
  SFX_Play(_Clickokay, 0);//aClickokay.Play
  if (player.ActiveInventory!=null)
  {
    player.ActiveInventory=null;
    mouse.Mode=eModeWalkto; 
  }
  gQuit.Y=359;//+gQuit.Height;
  int targetY = (360 - gQuit.Height)/2;
  gQuit.X = (640 - gQuit.Width)/2;
  gQuit.Visible=true;
  gQuit.TweenPosition(0.5, gQuit.X, targetY, eEaseOutTween, eBlockTween);
  
  gClear.Visible=true;
}

function oload_Interact()
{
  OpenSave(1);
  
  gSave.X=-gSave.Width;
  gSave.Y = (360 - gSave.Height)/2;
  int targetX = (640 - gSave.Width)/2;
  gSave.Visible=true;
  gSave.TweenPosition(0.5, targetX, gSave.Y, eEaseOutTween, eBlockTween);
  
  gClear.Visible=true;
}

function ooptions_Interact()
{  
  gOptions.Y=360+gOptions.Height;
  int targetY = (360 - gOptions.Height)/2;
  int targetX = (640 - gOptions.Width)/2;
  gOptions.X=targetX;
  gOptions.Visible=true;
  gOptions.TweenPosition(0.5, targetX, targetY, eEaseOutTween, eBlockTween);
  
  gClear.Visible=true;
}
