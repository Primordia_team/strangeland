// new module header
import function RainSplashes(Object*rainer, int raindrops, int updateinterval, int rainpathgraphic, int howmany, int ID);
import function RainUpdate(int Animation_Delay);
import function RainUpdateLight(int Animation_Delay, Object*farain, Object*midrain, Object*closerain);
import function PlayClip(AudioClip*toplay);
import function SetInvGraphic(int id, bool RestoreState=false);
import function RestoreInvs();
import function EgoCoverEars(float delay, bool yell=false);
import function Subtitler(this Character*,String text, int color=15);
import function SetMurmurProphecy(bool setTo);
import AudioClip* GetClip();
