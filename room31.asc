// room script file

bool LightningFlash=false;
bool grabSetting=false;
int randomBlinks;
int gotBlink=200;

function repeatedly_execute_always()
{  
  
  if (oClownzap.View==51)
  {
    int gf = oClownzap.Frame;
    if (gf==1 || gf==12|| gf==24|| gf==35
    || gf==44|| gf==57) 
    {
      if (!LightningFlash)
      {
        LightningSound(Random(100));   
        LightningFlash=true;
      }
    }
    else 
    {
      LightningFlash=false;
    }
  }
  else
  {
    LightningFlash=false;
  }
  
  if (cMouth.Animating==false)
  {
    cMouth.LockViewFrame(CDIDLE, 0, 0);
    cMouth.Animate(0, 3, eOnce, eNoBlock, eForwards);
  }
  randomBlinks++;
  if (randomBlinks>=gotBlink && cMouth.View!=CDTALK)
  {
    gotBlink=(Random(2)+3)*40;
    randomBlinks=0;
    cMouth.LockViewFrame(CDBLINK, 0, 0);
    cMouth.Animate(0, 3, eOnce, eNoBlock, eForwards);
  }
  
}


function room_Load()
{
  grabSetting=cEgo.ScaleMoveSpeed;
  cEgo.ScaleMoveSpeed=true;
 SetWalkBehindBase_New(1, 359);
  SetWalkBehindBase_New(2, 313);
  SetWalkBehindBase_New(3, 300);
  SetWalkBehindBase_New(4, 282);
  SetWalkBehindBase_New(5, 284);
  SetWalkBehindBase_New(6, 359);
  
  oClownzap.SetView(51, 0);
  oClownzap.Animate(0, 1, eRepeat, eNoBlock, eForwards);
  
  hHotspot3.Enabled=false;
  if (cEgo.PreviousRoom==-1)
  {
    cEgo.x=564;
    cEgo.y=292;
  }
  
  
  cMouth.Baseline=314;//3;
  cMouth.ChangeRoom(31);
  
  cMouth.SpeechView=CDTALK;
  
  
  cMouth.ChangeView(CDTALK);
  cMouth.x=223;//320 360
  
 oObjectX.Clickable=false;
 oObjectX.SetView(FACELIGHTS, 0, 0);
 oObjectX.Animate(0, 3, eRepeat, eNoBlock, eForwards);
 
 oobjectbg.Clickable=false;
 oobjectbg.SetView(FACELIGHTS, 1, 0);
 oobjectbg.Animate(1, 3, eRepeat, eNoBlock, eForwards);
}
int mouthtocall=1;

function Gaheehaw()
{
  //AudioChannel *channel = aLaugh1a.Play();
   // channel.Volume=35;
   SFX_Play(_Laugh1a, 0);//
    SFX_SetVolume(_Laugh1a, 35);
    if (SFX_GetVolume(_Laugh1a)>sldSound.Value) SFX_SetVolume(_Laugh1a, sldSound.Value);
    cMouth.Speak("Gaheeheehaw!");
    
    int grabB = cMouth.Baseline;
    cMouth.Baseline=10000;
    int toView=MOUTHTALK;
    cMouth.LockViewFrame(toView, cMouth.Loop, cMouth.Frame);
    cMouth.Animate(0, 4, eRepeat, eNoBlock, eForwards);
    
    //int de=0;
    /*
    while (channel.IsPlaying)
    {
      if (channel.Volume>=1) channel.Volume--;
      else 
      {
        channel.Stop();
        channel.Volume=0;
      }
      //de++;
      WaitC(1);
    }*/
    SFX_Stop(_Laugh1a, 1500);//
    WaitC(35);
    //cEgo.SayBackground(String.Format("%d",de));
    cMouth.LockViewFrame(toView, cMouth.Loop, 0);
    cMouth.UnlockView();
    cMouth.Baseline = grabB;
}

function MouthInteraction()
{
  //return;
  if (Game.DoOnceOnly("StrangelandEgoClownFirst"))
  {
    cMouth.Speak("Don't tell me you're expecting a joke?");
    cMouth.Speak("Fine.");
    cMouth.Speak("\"Don't worry, it's all in your...\"");
    cMouth.Speak("\"Sometimes you fall behind, sometimes you get...\"");
    cMouth.Speak("\"Wow, you're really in over your...\"");
    cMouth.Speak("*sigh*");
    cMouth.Speak("The thing is, you never know till the end,");
    cMouth.Speak("whether something's a comedy,");
    cMouth.Speak("or a tragedy.");
    cMouth.Speak("If the reverend doctor only brings the good word,");
    cMouth.Speak("Juliet wakes up");
    cMouth.Speak("in Romeo's arms.");
    cMouth.Speak("And not?");
    cMouth.Speak("\"Never was a story of more woe...\"");
  }
  else 
  {
    if (!EgoHead)
    {
      if (Game.DoOnceOnly("StrangelandEgoClown_WithHead"))
      {
        cMouth.Speak("Again?");
        cMouth.Speak("If you're expecting me to hop on your shoulders,");
        cMouth.Speak("you'll be waiting a long time.");
      }
      else 
      {
        cMouth.Speak("I don't know if you can hear me.");
        cMouth.Speak("But you really need to find yourself a head.");
      }
    }
    else 
    {
      cMouth.Speak("Well, I'm glad we're seeing eye to eye.");
    }
  }
 
 
  //if (Game.DoOnceOnly("RollTheTongue_Deadland"))
  //{
  //  SFX_Play(_ATongueroll1a, 0);//aTongueroll1a.Play();
  //  otongue.Animate(0, 3, eOnce, eBlock, eForwards);
  //}
  
  cEgo.Baseline=0;

}


function room_AfterFadeIn()
{
  PlayClip(aFace1a);
  
    StartCutscene(eSkipESCOnly);
    cEgo.Walk(389, 351, eBlock, eAnywhere);
  
  
    cEgo.FaceRotate(192, cEgo.y-300, true);        
    cEgo.Baseline=5000;
    MouthInteraction();
    cEgo.Baseline=0;
    EndCutscene();
 

}



function hHotspot1_Interact()
{
 // MouthInteraction();  
  hHotspot5.RunInteraction(eModeInteract);
}

function hHotspot2_Interact()
{
  cEgo.Walk(189, 355, eBlock, eWalkableAreas);
  cEgo.FaceDirection(eDirectionUp, eBlock);
  
  if (Game.DoOnceOnly("postermouth_Deadland"))
  {
    cEgo.Speak("It's an ad for \"Balm of Gilead(tm).\"");
    cEgo.Speak("There's a photo of a mysteriously beautiful woman");
    cEgo.Speak("with the caption, \"Is there?\""); 
    WaitC(20);
    cEgo.Speak("Or is she beautifully mysterious?");
  }
  else 
  {
    cEgo.Speak("It's an ad for \"Balm of Gilead(tm).\"");
  }  

}

function hHotspot2_Look()
{
  hHotspot2_Interact();
}

function hHotspot1_Look()
{
  cEgo.FaceRotate(getmx, getmy, true);
  cEgo.Speak("Not very inviting.");
}

function hHotspot3_Interact()
{
  cEgo.FaceRotate(getmx, getmy, true);
  cEgo.Speak("It just says \"inside.\"");  
}

function hHotspot3_Look()
{
  hHotspot3.RunInteraction(eModeInteract);
}

function hHotspot4_Interact()
{
  if (!gotdoubleclick)cEgo.Walk(getmx, getmy, eBlock, eWalkableAreas);
  if (!gotdoubleclick)cEgo.FaceDirection(eDirectionDown);
  gFade.TweenTransparency(0.5, 0, eEaseOutTween, eBlockTween);
  cEgo.ChangeRoom(32,280, 229);// 517, 163);
}

function hHotspot5_Interact()
{
  
  cEgo.FaceRotate(192, cEgo.y-300, true);        

  cEgo.Walk(293, 285, eBlock, eWalkableAreas);
  
  
  gFade.Visible=true;
  gFade.Transparency=100;
  gFade.TweenTransparency(2.5, 0, eEaseOutTween, eNoBlockTween);
  cEgo.Walk(273, 255, eBlock, eAnywhere);
  WaitC(40);
  cEgo.ChangeRoom(30, 456, 450);// );
  
  
}

function hHotspot0_Interact()
{
}

function hHotspot0_Look()
{

}

function hHotspot5_Look()
{
hHotspot1.RunInteraction(eModeLookat);
}

function hHotspot1_UseInv()
{
 hHotspot5.RunInteraction(eModeUseinv);
}

function hHotspot5_UseInv()
{
cEgo.FaceRotate(getmx, getmy, true);
  cMouth.Speak("That's not funny.");
}

function room_Leave()
{
cEgo.ScaleMoveSpeed=grabSetting;
}
