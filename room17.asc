// room script file



function SetPoser(int x, int y)
{
  cFalling.x=x;
  cFalling.y=y;
  WaitC(1);
}


bool fifthmove=false;



function room_Load()
{
  if (cEgo.PreviousRoom==-1)
  {
    hasnotpressedstart=false;
  }
  SetBackgroundFrame(1);
  cFalling.ManualScaling=true;
  cFalling.Scaling=78;
  obganim.SetView(FALLBG, 0, 0);
  cFalling.LockViewFrame(FALLINGROT, 0, 0);
  cFalling.Animate(0, 3, eRepeat, eNoBlock, eForwards);
  cEgo.Transparency=100;
  cFalling.z=-20;
  cFalling.x=433;
  cFalling.y=180;  
  cFalling.Transparency=0;
  
  gFade.Transparency=0;
  gFade.Visible=true;
  
  
}

function room_AfterFadeIn()
{
  
  StartCutscene(eSkipESCOnly);
  gFade.TweenTransparency(3.6, 100, eLinearTween, eNoBlockTween);


SetPoser(433,180);

cFalling.TweenScaling(4.0, 30, eLinearTween, eNoBlockTween);
  obganim.Animate(0, 4, eOnce, eNoBlock, eForwards);


SetPoser(431,183);
SetPoser(431,183);
SetPoser(427,186);
SetPoser(426,186);
SetPoser(422,189);
SetPoser(420,190);
SetPoser(413,192);
SetPoser(410,192);
SetPoser(404,194);
SetPoser(395,195);
SetPoser(392,196);
SetPoser(381,197);
SetPoser(381,197);
SetPoser(374,198);
SetPoser(370,198);
SetPoser(362,198);
SetPoser(354,198);
SetPoser(351,198);
SetPoser(343,198);
SetPoser(340,198);
SetPoser(333,198);
SetPoser(329,198);
SetPoser(323,198);
SetPoser(316,198);
SetPoser(308,198);
SetPoser(303,197);
SetPoser(299,197);
SetPoser(288,195);
SetPoser(284,195);
SetPoser(275,193);
SetPoser(268,193);
SetPoser(265,192);
SetPoser(259,191);
SetPoser(253,188);
SetPoser(250,188);
SetPoser(245,186);
SetPoser(242,185);
SetPoser(237,183);
SetPoser(235,183);
SetPoser(233,181);
SetPoser(227,178);
SetPoser(223,175);
SetPoser(222,174);
SetPoser(220,171);
SetPoser(219,169);
SetPoser(217,165);
SetPoser(216,160);
SetPoser(215,157);
SetPoser(214,153);
SetPoser(214,150);
SetPoser(214,149);
SetPoser(214,148);
SetPoser(214,145);
SetPoser(214,142);
SetPoser(214,142);
SetPoser(214,139);
SetPoser(214,136);
SetPoser(215,134);
SetPoser(217,132);
SetPoser(218,130);
SetPoser(221,128);
SetPoser(223,126);
SetPoser(227,124);
SetPoser(229,123);
SetPoser(233,121);
SetPoser(236,119);
SetPoser(238,118);
SetPoser(243,117);
SetPoser(245,117);
SetPoser(247,117);

SetPoser(253,116);
SetPoser(259,116);
SetPoser(262,116);
SetPoser(265,116);
SetPoser(270,116);
SetPoser(273,116);
SetPoser(274,116);
SetPoser(277,116);
SetPoser(279,117);
SetPoser(280,118);
SetPoser(284,121);
SetPoser(287,123);
SetPoser(287,123);
SetPoser(290,126);
SetPoser(292,130);
SetPoser(293,132);
SetPoser(297,136);
SetPoser(298,137);



SetPoser(300,141);
SetPoser(302,143);
SetPoser(302,144);
SetPoser(302,146);
SetPoser(303,149);
SetPoser(303,152);
SetPoser(303,153);
SetPoser(303,155);
SetPoser(303,158);
SetPoser(303,159);
SetPoser(303,161);
SetPoser(303,162);
SetPoser(302,164);
SetPoser(301,165);
SetPoser(299,167);
SetPoser(297,168);
SetPoser(294,170);
SetPoser(292,170);
SetPoser(289,171);
SetPoser(286,172);
SetPoser(283,172);
SetPoser(280,172);
SetPoser(278,172);
SetPoser(272,173);
SetPoser(271,173);
SetPoser(269,173);
SetPoser(265,173);
SetPoser(263,173);

SetPoser(259,173);
SetPoser(256,172);
SetPoser(251,170);
SetPoser(246,168);
SetPoser(244,167);
SetPoser(239,163);
SetPoser(238,162);
SetPoser(237,159);
SetPoser(237,159);


gFade.StopAllTweens(eFinishTween);
gFade.Transparency=100;
gFade.Visible=true;
gFade.TweenTransparency(1.2, 0, eLinearTween, eNoBlockTween);


cFalling.TweenTransparency(1.0, 75, eEaseInTween, eNoBlockTween);//100
 fifthmove=true;

SetPoser(237,156);
SetPoser(237,152);
SetPoser(237,151);
SetPoser(237,149);
SetPoser(237,148);
SetPoser(237,146);
SetPoser(237,145);
SetPoser(237,143);


SetPoser(237,142);
SetPoser(238,140);
SetPoser(239,138);
SetPoser(240,137);
SetPoser(241,136);
SetPoser(242,135);
SetPoser(244,134);
SetPoser(245,134);
SetPoser(246,133);
SetPoser(247,131);
SetPoser(248,131);
SetPoser(250,130);
SetPoser(251,130);
SetPoser(252,129);
SetPoser(254,128);
SetPoser(255,128);
SetPoser(256,128);
SetPoser(258,127);
SetPoser(259,127);
SetPoser(261,127);
SetPoser(263,126);
SetPoser(264,126);
SetPoser(265,125);
SetPoser(266,125);
SetPoser(268,125);
SetPoser(268,125);
SetPoser(268,125);
SetPoser(271,124);
SetPoser(272,124);
SetPoser(272,123);
SetPoser(272,123);
SetPoser(272,123);
SetPoser(273,123);
SetPoser(273,123);
SetPoser(274,123);
SetPoser(275,123);
SetPoser(275,123);
SetPoser(275,123);
SetPoser(275,123);
SetPoser(275,123);
SetPoser(275,123);
SetPoser(275,123);
SetPoser(275,123);
SetPoser(275,123);
SetPoser(276,123);
SetPoser(276,123);
SetPoser(276,123);
SetPoser(276,123);
SetPoser(276,123);
SetPoser(276,123);
SetPoser(276,123);
SetPoser(276,123);
SetPoser(276,123);
SetPoser(276,123);
SetPoser(277,123);
SetPoser(277,123);
SetPoser(277,123);
SetPoser(277,123);



//fifthmove=true;
}



function room_RepExec()
{
  if (fifthmove)
  {
   // if (Game.DoOnceOnly("runfallingRoom18"))
  //  {      
      //gFade.Transparency=100;
      //gFade.Visible=true;
      //gFade.TweenTransparency(1.2, 0, eLinearTween, eBlockTween);
      
      diedfromwell=true;
      EndCutscene();
      cEgo.ChangeRoomz(1, 768, 235);
   // }
  }
  else 
  {
    WaitC(1);
  }
}

function room_Leave()
{
  cEgo.Transparency=0;
  fifthmove=false;
}
