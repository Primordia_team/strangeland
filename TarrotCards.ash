// new module header
enum TarrotCard {
  eBackOfDeck = 4284,
  eDeath = 4285,
  eFuture = 4286,
  eJudgement = 4287,
  ePast = 4288,
  ePresent = 4289,
  eStrength = 4290,
  eChariot = 4291,
  eDevil = 4292,
  eEmperor = 4293,
  eEmpress = 4294,
  eFool = 4295,
  eHangedMan = 4296,
  eHermit = 4297,
  eHierophant = 4298,
  eLovers =4299,
  eMagician = 4300,
  eMoon = 4301,
  ePriestess = 4302,
  eStar = 4303,
  eSun = 4304,
  eTower = 4305,
  eWorld = 4306,
  eWheel = 4307,
};

import function LoadTarrotUI(TarrotCard tCard1, TarrotCard tCard2, TarrotCard tCard3, int option1, int option2, int option3);
import function ShowCards(TarrotCard tCard1, TarrotCard tCard2, TarrotCard tCard3, int option1, int option2, int option3);
import function CloseTarrotUI();
import function ClickTarrot(MouseButton button, int id);
