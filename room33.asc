// room script file

bool grabZ=false;

function room_AfterFadeIn()
{
  PlayClip(aValkFurLOOP);
  if (cEgo.PreviousRoom==28)
  {
    cEgo.Walk(288,73, eNoBlock, eAnywhere);
  }
  else if (cEgo.PreviousRoom==29) cEgo.Walk(500, 124, eNoBlock, eAnywhere);
  
}

function room_Load()
{
  SetWalkBehindBase_New(1, 359);
  SetWalkBehindBase_New(2, 135);
  SetWalkBehindBase_New(3, 142);
  SetWalkBehindBase_New(4, 126);
  SetWalkBehindBase_New(5, 46);
  SetWalkBehindBase_New(6, 86);  
  SetWalkBehindBase_New(7, 99);
  SetWalkBehindBase_New(8, 81);
  if (cEgo.PreviousRoom==-1)
  {
    hasnotpressedstart=false;
  }
  
  grabZ=cEgo.ScaleMoveSpeed;
  cEgo.ScaleMoveSpeed=true;
  
  oblights.SetView(BROKENLIGH, 0, 0);
  oblights.Animate(0, 3, eRepeat, eNoBlock, eForwards);
  
  
  cEgo.ManualScaling=true;
  if (cEgo.PreviousRoom==28) cEgo.Scaling=GetScalingAt(286, 69);
  else cEgo.Scaling=GetScalingAt(573, 98);  
}


function repeatedly_execute_always() 
{
  if (GetWalkableAreaAt(player.x, player.y)!=0)
  {
    if (cEgo.ManualScaling)
    {
      cEgo.ManualScaling=false;
    }
  }
}

function hHotspot1_Interact()
{
  cEgo.ChangeRoomz(29, 104-80, 348+100);
//  cEgo.ChangeRoom(5, -70, 251);
}

function hHotspot2_Interact()
{
  cEgo.ChangeRoomz(28,554, 530);// 700, 330);491, 352
}

function room_Leave()
{
cEgo.ScaleMoveSpeed=grabZ;
}
