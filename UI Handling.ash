// new module header
import function Save(int id);
import function SetUpDialog(Dialog*getDialog);
import function DialogControls(float dur, int value, bool visibility=true);
import function OpenSave(int save=0);
import function SaveScrollUp();
import function SaveScrollDown();
import function ActuallySave();
import function EraseSave();
import function SaveScroll(int val);
import function CustomUseInv(InventoryItem*touseon);
import bool NoDialogOptions(Dialog*tocheck);
import function ActuallyLoad();
import function GenericDeath();
import function TeratomaEye(Button *touse, MouseButton button);
import function TeratomaBegin();
import bool CheckIfSaveGamesExist();