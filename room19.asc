// room script file

int grabPr=7;
function room_Load()
{
  DisableAutosave=true;
  hasnotpressedstart=false;
  gFade.Transparency=0;
  gFade.Visible=true;
  cEgo.Transparency=100;
  SeenBisectedhead=true;
  if (cEgo.PreviousRoom!=-1)grabPr=cEgo.PreviousRoom;
  else cEgo.ChangeRoom(7);
  
  obisheadanim.SetView(BISHEADANI, 0, 0);
  obisheadanim.Animate(obisheadanim.Loop, 3, eRepeat, eNoBlock, eForwards);
}

function room_Leave()
{
  gFade.Transparency=100;
  gFade.Visible=false;
  cEgo.Transparency=0;
}

function room_AfterFadeIn()
{
 
    
 
  
 StartCutscene(eSkipESCOnly); 
 gFade.TweenTransparency(1.2, 100, eLinearTween, eBlockTween);
 
 obisheadanim.TweenTransparency(1.0, 100, eEaseInEaseOutTween, eBlockTween);
 
 if (visioncount==0) cEgo.Subtitler("   help");
 else if (visioncount==1) cEgo.Subtitler(" help me");
 else if (visioncount>=2) cEgo.Subtitler("help me, please");
 visioncount++; 
 //WaitC(40);
 float timexx=4.0;
 
 oLeftside.TweenPosition(timexx, -100, oLeftside.Y, eLinearTween, eNoBlockTween); 
 oRightside.TweenPosition(timexx, 38, oRightside.Y, eLinearTween, eBlockTween); 
 WaitC(150);
 
 Music_Stop(1000);
 gFade.Transparency=100;
 gFade.Visible=true;
 gFade.TweenTransparency(1.2, 0, eLinearTween, eBlockTween);
 EndCutscene();
 // ..player.ChangeRoom(grabPr);
 cEgo.LockViewFrame(EGOGROAN, 0, 0); 
 cEgo.SetAsPlayer();
}